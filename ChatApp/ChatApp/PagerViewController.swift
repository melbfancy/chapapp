//
//  PagerViewController.swift
//  SnapchatCrossPager
//
//  Created by Paride on 13/09/2016. Modified by Chang Hsien Liu.
//  Copyright © 2016 paridebroggi. All rights reserved.
//

import UIKit

class PagerViewController: UIViewController {
    
    enum ScrollDirection {
        case horizontal
        case vertical
    }
    
    var leftVC: UIViewController {
        return beforeVC
    }
    
    var rightVC: UIViewController {
        return afterVC
    }
    
    var centerVC: UIViewController {
        return middleVC
    }
    
    var topVC: UIViewController {
        return beforeVC
    }
    
    var bottomVC: UIViewController {
        return afterVC
    }
    
    var scrollDirection: ScrollDirection
    var beforeVC: UIViewController
    var middleVC: UIViewController
    var afterVC: UIViewController
    var rightEndVC: UIViewController
    var viewControllers: [UIViewController]
    var totalVCcount: CGFloat
    var scrollView: UIScrollView!
    
    init(scrollDirection: ScrollDirection = .horizontal, beforeVC: UIViewController, centerVC: UIViewController, afterVC: UIViewController, endVC: UIViewController?) {
        self.scrollDirection = scrollDirection
        self.beforeVC = beforeVC
        self.middleVC = centerVC
        self.afterVC = afterVC
        if let vc = endVC {
            self.rightEndVC = vc
        } else {
            self.rightEndVC = UIViewController()
            self.rightEndVC.view.tag = -1
        }
        self.viewControllers = [beforeVC, middleVC, afterVC, rightEndVC]
        self.totalVCcount = CGFloat(self.viewControllers.count)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
    }
    
}

//MARK: - PREPARING VIEW
extension PagerViewController {
    
    func prepareView(){
        scrollView = UIScrollView(frame: view.bounds)
        scrollView.isPagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.bounces = false
        view.addSubview(scrollView)
        if scrollDirection == .horizontal {
            prepareHorizontalScrollView(width: scrollView.bounds.width, height: scrollView.bounds.height)
        }
        else {
            prepareVerticalScrollView(width: scrollView.bounds.width, height: scrollView.bounds.height)
        }
        viewControllers.forEach{ viewController in
            addChildViewController(viewController)
            scrollView.addSubview(viewController.view)
            viewController.didMove(toParentViewController: self)
        }
    }
    
    private func prepareHorizontalScrollView(width: CGFloat, height: CGFloat) {
        scrollView.contentSize = CGSize(width: width * totalVCcount, height: height)
        beforeVC.view.frame.origin = CGPoint(x: 0, y: 0)
        middleVC.view.frame.origin = CGPoint(x: width, y: 0)
        afterVC.view.frame.origin = CGPoint(x: width * (totalVCcount - 2), y: 0)
        rightEndVC.view.frame.origin = CGPoint(x: width * (totalVCcount - 1), y: 0)
        scrollView.contentOffset.x = middleVC.view.frame.origin.x
    }

    private func prepareVerticalScrollView(width: CGFloat, height: CGFloat){
        scrollView.contentSize = CGSize(width: width, height: height * (totalVCcount - 1) )
        beforeVC.view.frame.origin = CGPoint(x: 0, y: 0)
        middleVC.view.frame.origin = CGPoint(x: 0, y: height)
        afterVC.view.frame.origin = CGPoint(x: 0, y: height * (totalVCcount - 2))
        scrollView.contentOffset.y = middleVC.view.frame.origin.y
    }
    
}

//MARK: - DELEGATE
extension PagerViewController: SnapchatCrossPagerDelegate {
    
    func scrollViewShouldScroll() -> Bool {
        if scrollDirection == .vertical &&
            (scrollView.contentOffset.y < middleVC.view.frame.origin.y || scrollView.contentOffset.y > middleVC.view.frame.origin.y) {
            return false
        } else {
            return true
        }
    }
    
}

//
//  UserStoryTableViewCell.swift
//  ChatStories
//
//  Created by Liu Sam on 10/9/16.
//  Copyright © 2016 Liu Sam. All rights reserved.
//

import UIKit

class UserStoryTableViewCell: UITableViewCell {

    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var userProfileImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

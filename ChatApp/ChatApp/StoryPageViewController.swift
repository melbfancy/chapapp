//
//  StoryPageViewController.swift
//  ChatStories
//
//  Created by Liu Sam on 10/4/16.
//  Copyright © 2016 Liu Sam. All rights reserved.
//

import UIKit

class StoryPageViewController: UIPageViewController {
    
    var stories: Stories!
    var currentPage = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action:#selector(userTapped)))
        
        setViewControllers([viewControllerAtIndex(index: 0)!], direction: .forward, animated: true, completion: nil)
        
        dataSource = self
        delegate = self
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
     func userTapped() {
        print("SAM: tapped")
     }
    
}

extension StoryPageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        currentPage -= 1
        if let currentPageViewController = viewController as? StoryContentViewController, currentPageViewController.pageIndex > 0 {
            return viewControllerAtIndex(index: currentPageViewController.pageIndex - 1)
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        currentPage += 1
        if let currentPageViewController = viewController as? StoryContentViewController, currentPageViewController.pageIndex < stories.size - 1 {
            return viewControllerAtIndex(index: currentPageViewController.pageIndex + 1)
        }
        return nil
    }

    
    func viewControllerAtIndex(index: Int) -> UIViewController? {
        if let story = stories.story(at: index) {
            let storyboard = UIStoryboard(name: "Discover", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "StoryContentVC") as! StoryContentViewController
            vc.applyData(pageIndex: index, storyData: story, userID: (stories.user?.id)!)
            if stories.user?.email == nil {
                vc.isPublic = true
            } else {
                vc.isPublic = false
            }
            return vc
        }
        
        return nil
    }
}

extension StoryPageViewController: UIPageViewControllerDelegate{
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {

    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {

    }
}


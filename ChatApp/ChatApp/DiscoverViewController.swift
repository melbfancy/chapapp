//
//  DiscoverViewController.swift
//  ChatStories
//
//  Created by Liu Sam on 10/1/16.
//  Copyright © 2016 Liu Sam. All rights reserved.
//

import UIKit
import Firebase

class DiscoverViewController: UIViewController {
    
    
    @IBOutlet weak var storyCollectionView: UICollectionView!
    
    
    fileprivate let reuseIdentifier = "StoryCell"
    fileprivate let sectionInsets = UIEdgeInsets(top: 15.0, left: 15.0, bottom: 15.0, right: 15.0)
    fileprivate let itemsPerRow: CGFloat = 3
    
    var stories = [Stories]()
    
    override func viewDidLoad() {
        super.viewDidLoad()       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func showStory(_ sender: AnyObject) {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.snapchatCrossPagerViewController?.scrollToPage(page: 2)
        }
    }
    
    func setupUserSpecificAspects() {
        // suggested story
        if let uid = FIRAuth.auth()?.currentUser?.uid {
            DataService.ds.REF_SUBSCRIBE.child(uid).observe(.value, with: { (snapshot) in
                var subscribeList = [String]()
                if let snapshot = snapshot.children.allObjects as? [FIRDataSnapshot] {
                    for snap in snapshot {
                        subscribeList.append(snap.key)
                    }
                }
                
                DataService.ds.REF_STORY_PUBLIC.queryOrdered(byChild: "timestamp").observe(.value, with: { (snapshot) in
                    if let snapshot = snapshot.children.allObjects as? [FIRDataSnapshot] {
                        self.stories.removeAll()
                        for snap in snapshot {
                            if let snapshot2 = snap.children.allObjects as? [FIRDataSnapshot], !subscribeList.contains(snap.key){
                                var storyArr = [Story]()
                                for story in snapshot2{
                                    if let storyDict = story.value as? Dictionary<String, AnyObject> {
                                        let key = story.key
                                        let story = Story(storyId: key, storyData: storyDict)
                                        storyArr.append(story)
                                    }
                                }
                                
                                DataService.ds.REF_USER_PUBLIC.child(snap.key).observeSingleEvent(of: .value, with: { (snapshot) in
                                    if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                                        let user = User()
                                        user.id = snapshot.key
                                        user.name = userDict["name"] as? String
                                        let storiesItem = Stories(user: user, storiesData: storyArr)
                                        if let rating = userDict["rating"] as? Int {
                                            storiesItem.rating = rating
                                        } else {
                                            storiesItem.rating = 0
                                        }
                                        self.stories.append(storiesItem)
                                        self.storyCollectionView?.reloadData()
                                    }
                                })
                            }
                        }
                    }
                })
            })
        }
    }
    
    func sortByRating() {
        stories.sort {
            $0.rating > $1.rating
        }
    }
}

extension DiscoverViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return stories.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        sortByRating()
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier,
                                                      for: indexPath) as! StoryCollectionViewCell
        cell.backgroundColor = UIColor.gray
        if let story = stories[indexPath.item].story(at: 0) {
            cell.titleLabel.text = story.title
            cell.userLabel.text = stories[indexPath.item].user?.name
            cell.backgroundImage?.loadImageUsingCacheWithUrlString(urlString: story.media)
        }
        // Configure the cell
        return cell
    }
}

extension DiscoverViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Discover", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "StoryPageVC") as! StoryPageViewController
        vc.stories = stories[indexPath.item]
        let userID = stories[indexPath.item].user?.id
        DataService.ds.increaseUserRating(userID: userID!, byValue: 1)
        vc.modalPresentationStyle = UIModalPresentationStyle.currentContext
        present(vc, animated: true, completion: nil)
    }
}

extension DiscoverViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}

//
//  FriendCell.swift
//  ChatApp
//
//  Created by Fan Qi on 16/10/8.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import UIKit
import Firebase

class FriendCell: UITableViewCell {
    
    var buttonDelegate: FriendCellButtonProtocol?
    
    var user: User?
    
    let activityIndicatorView: UIActivityIndicatorView = {
        let aiv = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        aiv.translatesAutoresizingMaskIntoConstraints = false
        aiv.hidesWhenStopped = true
        return aiv
    }()
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 24
        imageView.layer.masksToBounds = true
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        return imageView
    }()
    
    lazy var requestButton: UIButton = {
        let button = UIButton(type: UIButtonType.system)
        button.backgroundColor = UIColor(r: 21, g: 139, b: 201)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.tintColor = UIColor.white
        button.layer.cornerRadius = 10
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(handleOperation), for: UIControlEvents.touchUpInside)
        return button
    }()
    
    func handleOperation() {
        if let targetUser = user, let delegate = buttonDelegate {
            activityIndicatorView.startAnimating()
            delegate.performOperation(user: targetUser)
            Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { (timer) in
                self.activityIndicatorView.stopAnimating()
            })
        }
    }
    
    func setupCell() {
        textLabel?.text = user?.name
        detailTextLabel?.text = user?.email
        if let profileImageUrl = user?.profileImageUrl {
            profileImageView.loadImageUsingCacheWithUrlString(urlString: profileImageUrl)
        }
    }
    
    func setupButton(title: String) {
        requestButton.setTitle(title, for: UIControlState.normal)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        textLabel?.frame = CGRect(x: 64, y: textLabel!.frame.origin.y - 2, width: textLabel!.frame.width, height: textLabel!.frame.height)
        
        detailTextLabel?.frame = CGRect(x: 64, y: detailTextLabel!.frame.origin.y + 2, width: detailTextLabel!.frame.width, height: detailTextLabel!.frame.height)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: UITableViewCellStyle.subtitle, reuseIdentifier: reuseIdentifier)
        
        addSubview(profileImageView)
        addSubview(requestButton)
        addSubview(activityIndicatorView)
        
        //need x,y,width,height anchors
        profileImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        profileImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 48).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant: 48).isActive = true
        
        //need x,y,width,height anchors
        requestButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20).isActive = true
        requestButton.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        requestButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        requestButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
        
        
        activityIndicatorView.centerXAnchor.constraint(equalTo: requestButton.centerXAnchor).isActive = true
        activityIndicatorView.centerYAnchor.constraint(equalTo: requestButton.centerYAnchor).isActive = true
        activityIndicatorView.widthAnchor.constraint(equalToConstant: 15).isActive = true
        activityIndicatorView.heightAnchor.constraint(equalToConstant: 15).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

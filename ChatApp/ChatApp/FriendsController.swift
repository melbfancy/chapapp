//
//  FriendsController.swift
//  ChatApp
//
//  Created by Fan Qi on 16/10/14.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import UIKit
import Firebase

class FriendsController: UITableViewController, UISearchResultsUpdating {
    
    var imageToSend: UIImage?
    
    var timeLimit: Int?
    
    var timer: Timer?
    
    let cellId = "cellId"
    
    var users = [User]()
    
    var filteredUsers = [User]()
    
    var usersDictionary = [String: [User]]()
    
    var keysAndValuesArray = [[Any]]()
    
    var messagesController: MessagesController?
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var searchText: String? {
        didSet {
            handleReloadTable()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(handleCancel))
        
        tableView.register(UserCell.self, forCellReuseIdentifier: cellId)
        
        fetchUser()
    }
    
    func filterUsers() {
        filteredUsers = users.filter { (user) -> Bool in
            if searchText == nil || searchText == "" {
                return true
            } else {
                return user.name!.lowercased().contains(searchText!.lowercased())
            }
        }
    }
    
    func fetchUser(){
        guard let uid = FIRAuth.auth()?.currentUser?.uid else {
            return
        }
        let ref = FIRDatabase.database().reference().child("relations").child(uid)
        ref.observe(FIRDataEventType.childAdded, with: { (snapshot) in
            let userId = snapshot.key
            let userRef = FIRDatabase.database().reference().child("users").child(userId)
            userRef.observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                if let dictionary = snapshot.value as? [String: AnyObject] {
                    let user = User()
                    user.id = snapshot.key
                    // a convinient way to set all the properties
                    // if unmatched, will crash
                    user.setValuesForKeys(dictionary)
                    self.users.append(user)
                    self.attemptReloadOfTable()
                }
            })
        })
    }
    
    func setupUsersDictionary() {
        usersDictionary = [String: [User]]()
        for user in filteredUsers {
            if let key = user.name?.characters.first?.description.uppercased() {
                if usersDictionary[key] != nil {
                    usersDictionary[key]?.append(user)
                } else {
                    usersDictionary[key] = [user]
                }

                if let timestamp = messagesController?.messagesDictionary[user.id!]?.timestamp {
                    
                    if Int(Date().timeIntervalSince1970) - timestamp.intValue < 86400 {
                        if usersDictionary["Recents"] != nil {
                            usersDictionary["Recents"]?.append(user)
                        } else {
                            usersDictionary["Recents"] = [user]
                        }
                    }
                }
            }
        }
    }
    
    func setupKeysAndValuesArrays(){
        keysAndValuesArray = [[Any]]()
        for (key, value) in usersDictionary {
            if key == "Recents" {
                let newValue = value.sorted(by: { (user1, user2) -> Bool in
                    let user1Time = messagesController?.messagesDictionary[user1.id!]?.timestamp?.intValue
                    let user2Time = messagesController?.messagesDictionary[user2.id!]?.timestamp?.intValue
                    return user1Time! > user2Time!
                })
                keysAndValuesArray.append([key, newValue])
            } else {
                keysAndValuesArray.append([key, value])
            }
        }
        
        keysAndValuesArray.sort { (array1, array2) -> Bool in
            if array1.first as! String == "Recents" {
                return true
            } else if array2.first as! String == "Recents" {
                return false
            } else {
                return (array1.first as! String) < (array2.first as! String)
            }
        }
    }
    
    private func attemptReloadOfTable() {
        self.timer?.invalidate()
        
        self.timer = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(self.handleReloadTable), userInfo: nil, repeats: false)
    }
    
    func handleReloadTable() {
        filterUsers()
        setupUsersDictionary()
        setupKeysAndValuesArrays()
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func handleCancel(){
        dismiss(animated: true, completion: nil)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return keysAndValuesArray.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return keysAndValuesArray[section].first as? String
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (keysAndValuesArray[section].last as! [User]).count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! UserCell
        
        let user = (keysAndValuesArray[indexPath.section].last as! [User])[indexPath.row]
        
        if keysAndValuesArray[indexPath.section].first as? String == "Recents" {
            if let seconds = messagesController?.messagesDictionary[user.id!]?.timestamp?.doubleValue {
                let timestampDate = Date(timeIntervalSince1970: seconds)
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "hh:mm:ss a"
                cell.timeLabel.text = dateFormatter.string(from: timestampDate)
            }
        }
        cell.textLabel?.text = user.name
        cell.detailTextLabel?.text = user.email
        if let profileImageUrl = user.profileImageUrl {
            cell.profileImageView.loadImageUsingCacheWithUrlString(urlString: profileImageUrl)
        }
        return cell
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchController.isActive = false
        let user = (self.keysAndValuesArray[indexPath.section].last as! [User])[indexPath.row]
        dismiss(animated: false) {
            if let image = self.imageToSend, let limit = self.timeLimit {
                let chatLogController = ChatLogController(collectionViewLayout: UICollectionViewFlowLayout())
                chatLogController.user = user
                chatLogController.uploadToFirebaseStorageUsingImage(image: image, timeLimit: limit)
            } else {
                self.messagesController?.showChatController(for: user)
            }
        }
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        searchText = searchController.searchBar.text!
    }

}

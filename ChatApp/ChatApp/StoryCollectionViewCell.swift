//
//  StoryCollectionViewCell.swift
//  ChatStories
//
//  Created by Liu Sam on 10/4/16.
//  Copyright © 2016 Liu Sam. All rights reserved.
//

import UIKit

class StoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var backgroundImage: UIImageView!
}

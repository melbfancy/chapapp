//
//  MemoryStoriesController.swift
//  ChatApp
//
//  Created by Fan Qi on 16/10/17.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import UIKit
import Firebase

class MemoryStoriesController: UIViewController, UserSpecificSetupProtocol {
    
    var memoryStories = [MemoryStory]()
    var imageViews = [MemoryStoryImageView]()

    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupGeneralAspects()
        // setupUserSpecificAspects()
    }
    
    func setupGeneralAspects() {
        scrollView.frame = view.bounds
        scrollView.backgroundColor = UIColor.clear
        scrollView.contentSize = view.bounds.size
        scrollView.contentOffset = CGPoint(x: 0, y: 0)
    }
    
    func setupUserSpecificAspects(){
        for imageView in imageViews {
            imageView.removeFromSuperview()
        }
        memoryStories = [MemoryStory]()
        imageViews = [MemoryStoryImageView]()
        fetchMemoryStories()

    }
    
    func fetchMemoryStories() {
        guard let uid = FIRAuth.auth()?.currentUser?.uid else {
            return
        }
        
        let userMemoriesRef = FIRDatabase.database().reference().child("user-memory-stories").child(uid)
        userMemoriesRef.observe(FIRDataEventType.childAdded, with: { (snapshot) in
            let memoryStoryId = snapshot.key
            let memoryStoriesRef = FIRDatabase.database().reference().child("memory-stories").child(memoryStoryId)
            memoryStoriesRef.observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                guard let dictionary = snapshot.value as? [String: AnyObject] else {
                    return
                }
                
                let memoryStory = MemoryStory()
                memoryStory.id = memoryStoryId
                memoryStory.dict = dictionary
                self.memoryStories.append(memoryStory)
                
                let imageView = MemoryStoryImageView()
                let imageWidth = self.view.bounds.width / 2
                let imageHeight = self.view.bounds.width / 2
                let index = self.memoryStories.count - 1
                imageView.memoryStory = memoryStory
                imageView.isUserInteractionEnabled = true
                imageView.contentMode = .scaleAspectFill
                imageView.backgroundColor = UIColor.black
                imageView.layer.cornerRadius = imageWidth / 2
                imageView.layer.masksToBounds = true
                imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleTap(recognizer: ))))
                self.imageViews.append(imageView)
                
                DispatchQueue.main.async {
                    imageView.frame = CGRect(x: CGFloat(index % 2) * imageWidth, y: CGFloat(index / 2) * imageHeight , width: imageWidth, height: imageHeight)
                    self.scrollView.contentSize = CGSize(width: self.view.bounds.width, height: CGFloat(index / 2 + 1) * imageHeight)
                    self.scrollView.addSubview(imageView)
                }
            })
        })

    }
    
    func handleTap(recognizer: UITapGestureRecognizer) {
        if let targetView = recognizer.view as? MemoryStoryImageView {
            let controller = MemoryStoryDisplayController()
            controller.memoryStory = targetView.memoryStory
            present(controller, animated: false, completion: nil)
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

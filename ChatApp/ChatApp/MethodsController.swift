//
//  MethodsController.swift
//  ChatApp
//
//  Created by Fan Qi on 16/10/8.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import UIKit

class MethodsController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
        
    let method1ImageView = UIImageView()
    let method1Text = "Add by UserName"
    
    let method2ImageView = UIImageView()
    let method2Text = "Add by Snapcode"
    
    let method3ImageView = UIImageView()
    let method3Text = "Add by Snapcode Scanning"
    
    let method4ImageView = UIImageView()
    let method4Text = "Share Username"
    
    var methods = [Method]()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "reuseIdentifier")
        
        let method1 = Method()
        method1.imageView = method1ImageView
        method1.text = method1Text
        let method2 = Method()
        method2.imageView = method2ImageView
        method2.text = method2Text
        let method3 = Method()
        method3.imageView = method3ImageView
        method3.text = method3Text
        let method4 = Method()
        method4.imageView = method4ImageView
        method4.text = method4Text
        
        methods.append(method1)
        methods.append(method2)
        methods.append(method3)
        methods.append(method4)

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return methods.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        //cell.imageView = methods[indexPath.row].first
        cell.textLabel?.text = methods[indexPath.row].text
        // Configure the cell...
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let resultsController = ResultsController()
            resultsController.method = methods[indexPath.row]
            navigationController?.pushViewController(resultsController, animated: true)
        } else if indexPath.row == 1 {
            handlePickBarcode()
        } else if indexPath.row == 2 {
            let barcodeScannerController = BarcodeScannerController()
            navigationController?.pushViewController(barcodeScannerController, animated: true)
        } else {
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                if let url = appDelegate.currentUser?.barcodeImageUrl {
                    let text = "Add me on ChatApp " + url
                    
                    let activityViewController = UIActivityViewController(activityItems: [text], applicationActivities: nil)
                    
                    activityViewController.popoverPresentationController?.sourceView = self.view
                    
                    // exclude some activity types from the list (optional)
                    activityViewController.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.saveToCameraRoll]
                    
                    // present the view controller
                    present(activityViewController, animated: true, completion: nil)
                }
            }
        }
    }
    
    // action handlers
    func handlePickBarcode() {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = false
        present(picker, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }

    // image picker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var selectedImageFromPicker: UIImage?
        let singleResultController = SingleResultController()
        if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            selectedImageFromPicker = originalImage
        }
        if let selectedImage = selectedImageFromPicker {
            let text = QRBarcodeHelper.parseQRcode(from: selectedImage)
            if text != "" {
                singleResultController.userId = text
                singleResultController.navigationItem.title = text
                dismiss(animated: true, completion: {
                    self.navigationController?.pushViewController(singleResultController, animated: true)
                })
            }
        }
    }
}

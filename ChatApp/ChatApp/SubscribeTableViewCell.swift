//
//  SubscribeTableViewCell.swift
//  ChatStories
//
//  Created by Liu Sam on 10/9/16.
//  Copyright © 2016 Liu Sam. All rights reserved.
//

import UIKit

class SubscribeTableViewCell: UITableViewCell {


    @IBOutlet weak var subscribeCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

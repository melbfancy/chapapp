//
//  Extensions.swift
//  ChatApp
//
//  Created by Fan Qi on 16/9/28.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import UIKit

let imageCache = NSCache<NSString, UIImage>()

extension UIImageView {
    
    func loadImageUsingCacheWithUrlString(urlString: String) {
        
        self.image = nil
        
        //check cache for image first
        if let cachedImage = imageCache.object(forKey: urlString as NSString) {
            self.image = cachedImage
            return
        }
        
        if let url = URL(string: urlString) {
            //otherwise fire off a new download
            URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                //download hit an error so lets return out
                if error != nil {
                    return
                }
                
                DispatchQueue.main.async {
                    if let downloadedImage = UIImage(data: data!) {
                        imageCache.setObject(downloadedImage, forKey: urlString as NSString)
                        
                        self.image = downloadedImage
                    }
                }
            }).resume()
        }
    }
}

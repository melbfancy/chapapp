//
//  FriendCellButtonProtocol.swift
//  ChatApp
//
//  Created by Fan Qi on 16/10/8.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import Foundation

protocol FriendCellButtonProtocol {
    func performOperation(user: User)
}

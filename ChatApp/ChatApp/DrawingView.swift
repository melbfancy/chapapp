//
//  DrawingView.swift
//  ChatApp
//
//  Created by Fan Qi on 16/10/12.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import UIKit

class DrawingView: UIView {
    
    var currentPath: UIBezierPath?
    
    var paths = [UIBezierPath]() {
        didSet {
            setNeedsDisplay()
        }
    }

    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        UIColor.red.setStroke()
        currentPath?.stroke()
        // Drawing code
        for path in paths {
            path.stroke()
        }
    }


}

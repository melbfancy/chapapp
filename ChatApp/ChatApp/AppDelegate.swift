//
//  AppDelegate.swift
//  ChatApp
//
//  Created by Fan Qi on 16/9/26.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var reachability: Reachability?
    var snapchatCrossPagerViewController: SnapchatCrossPagerViewController?
    var leftController: MessagesController?
    var topController: NewFriendsController?
    var centerController: ContainerController?
    var rightController: StoryViewController?
    var rightEndController: DiscoverViewController?
    var bottomController: MemoriesController?
    var currentUser: User? {
        didSet {
            leftController?.setupUserSpecificAspects()
            topController?.setupUserSpecificAspects()
            rightController?.setupUserSpecificAspects()
            rightEndController?.setupUserSpecificAspects()
            bottomController?.childViewControllers.forEach({ (controller) in
                (controller as? UserSpecificSetupProtocol)?.setupUserSpecificAspects()
            })
        }
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        FIRApp.configure()
        
        leftController = MessagesController()
        topController = NewFriendsController()
        centerController = ContainerController()
        let storyboard = UIStoryboard(name: "Discover", bundle: nil)
        rightController = storyboard.instantiateViewController(withIdentifier: "StoryVC") as? StoryViewController
        rightEndController = storyboard.instantiateViewController(withIdentifier: "DiscoverVC") as? DiscoverViewController
        let memoriesStoryboard = UIStoryboard(name: "Memories", bundle: nil)
        bottomController = memoriesStoryboard.instantiateViewController(withIdentifier: "MemoriesController") as? MemoriesController
        
        let left = UINavigationController(rootViewController: leftController!)
        let right = rightController!
        let rightEnd = rightEndController!
        let top = UINavigationController(rootViewController: topController!)
        let bottom = UINavigationController(rootViewController: bottomController!)
        let center = centerController!
        
        snapchatCrossPagerViewController = SnapchatCrossPagerViewController(leftVC: left, centerVC: center, rightVC: right, topVC: top, bottomVC: bottom, endVC: rightEnd)
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        window?.rootViewController = snapchatCrossPagerViewController
        fetchAndSetCurrentUser()
        
        // network notification
        NotificationCenter.default.addObserver(self, selector: #selector(checkForReachability), name: NSNotification.Name.reachabilityChanged, object: nil)
        self.reachability = Reachability.forInternetConnection()
        self.reachability!.startNotifier()
        checkForReachability()
        return true
    }
    
    func checkForReachability() {
        let status = self.reachability?.currentReachabilityStatus()
        if status?.rawValue == NotReachable.rawValue {
            snapchatCrossPagerViewController?.handleNetworkDisconnected()
        } else {
            snapchatCrossPagerViewController?.handleNetworkConnected()
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func fetchAndSetCurrentUser() {
        if let uid = FIRAuth.auth()?.currentUser?.uid {
            let ref = FIRDatabase.database().reference().child("users").child(uid)
            ref.observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                if let dictionary = snapshot.value as? [String: AnyObject] {
                    let user = User()
                    user.id = snapshot.key
                    user.setValuesForKeys(dictionary)
                    self.currentUser = user
                }
            })
        }
    }

}


//
//  MemoryStoryDisplayController.swift
//  ChatApp
//
//  Created by Fan Qi on 16/10/17.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import UIKit

class MemoryStoryDisplayController: UIViewController {
    
    var memoryStory: MemoryStory?
    var imageViews = [UIImageView]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupGeneralAspects()
    }

    func setupGeneralAspects() {
        view.backgroundColor = UIColor.white
        if let dict = memoryStory?.dict {
            for (_, value) in dict {
                if let url = value as? String {
                    let imageView = UIImageView()
                    imageView.frame = view.bounds
                    imageView.contentMode = .scaleAspectFit
                    imageView.isUserInteractionEnabled = true
                    imageView.loadImageUsingCacheWithUrlString(urlString: url)
                    imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleTap(recognizer: ))))
                    view.addSubview(imageView)
                }
            }
        }
    }
    
    func handleTap(recognizer: UITapGestureRecognizer) {
        if let targetView = recognizer.view as? UIImageView {
            if view.subviews.count <= 1 {
                dismiss(animated: false, completion: nil)
            } else {
                targetView.removeFromSuperview()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

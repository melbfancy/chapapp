//
//  SingleResultController.swift
//  ChatApp
//
//  Created by Fan Qi on 16/10/10.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//


import UIKit
import Firebase

class SingleResultController: UITableViewController, FriendCellButtonProtocol {
    
    var userId: String? 
    
    var user : User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(FriendCell.self, forCellReuseIdentifier: "reuseIdentifier")
        tableView.allowsSelection = false
        fetchUser()
    }
    
    func fetchUser() {
        let ref = FIRDatabase.database().reference()
        if let id = validateUserId() {
            ref.child("users").child(id).observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                if let dictionary = snapshot.value as? [String: AnyObject] {
                    self.user = User()
                    self.user?.id = snapshot.key
                    self.user?.setValuesForKeys(dictionary)
                    self.tableView.reloadData()
                }
            })
        }
    }
    
    func validateUserId() -> String? {
        return userId
    }
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (user != nil) ? 1 : 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! FriendCell
        
        if user?.id == FIRAuth.auth()?.currentUser?.uid {
            cell.user = user
            cell.buttonDelegate = nil
            cell.setupButton(title: "Myself")
            cell.setupCell()
        } else {
            if let uid = FIRAuth.auth()?.currentUser?.uid, let toId = user?.id {
                let ref = FIRDatabase.database().reference().child("relations").child(uid).child(toId)
                ref.observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                    if let _ = snapshot.value as? Bool {
                        cell.user = self.user
                        cell.buttonDelegate = nil
                        cell.setupButton(title: "Added")
                        cell.setupCell()
                    } else {
                        cell.user = self.user
                        cell.buttonDelegate = self
                        cell.setupButton(title: "Add")
                        cell.setupCell()
                    }
                })
            }
        }

        return cell
    }
    
    func performOperation(user: User) {
        let ref = FIRDatabase.database().reference().child("requests")
        let childRef = ref.childByAutoId()
        
        if let toId = user.id, let fromId = FIRAuth.auth()?.currentUser?.uid {
            let timestamp = Int(Date().timeIntervalSince1970)
            let values = ["toId": toId, "fromId": fromId, "timestamp": timestamp] as [String : Any]
            
            childRef.updateChildValues(values) { (error, ref) in
                if error != nil {
                    return
                }
                
                let requestId = childRef.key
                
                let userRequestsRef = FIRDatabase.database().reference().child("user-requests").child(toId).child(fromId)
                userRequestsRef.updateChildValues([requestId: 1])
                
            }
        }
        
    }
    
}


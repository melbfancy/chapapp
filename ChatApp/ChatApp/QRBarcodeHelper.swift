//
//  QRBarcodeHelper.swift
//  ChatApp
//
//  Created by Fan Qi on 16/10/9.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import Foundation

import UIKit

class QRBarcodeHelper {
    
    static func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 10, y: 10)
            
            if let output = filter.outputImage?.applying(transform), let cgImage = CIContext(options: nil).createCGImage(output, from: output.extent) {
                return UIImage(cgImage: cgImage)
            }
        }
        
        return nil
    }
    
    static func parseQRcode(from image: UIImage) -> String? {
        let detector = CIDetector(ofType: CIDetectorTypeQRCode, context: nil, options: [CIDetectorAccuracy : CIDetectorAccuracyHigh])
        var decode = ""
        
        if let ciImage = CIImage(image: image) {
            if let features = detector?.features(in: ciImage), features.count != 0, let feature = features[0] as? CIQRCodeFeature {
                decode = feature.messageString ?? ""
            }
        }
        return decode
    }
    
}

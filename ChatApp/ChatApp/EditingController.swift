//
//  EditingController.swift
//  camera
//
//  Created by shengshuo zhang on 2016/10/4. Modified by Qi Fan.
//  Copyright © 2016年 shengshuo zhang. All rights reserved.
//

import UIKit
import Firebase

class EditingController: UIViewController, UITextFieldDelegate, UIPopoverPresentationControllerDelegate {

    var redirectDelegate: ContainerController?
    var image: UIImage?
    var video: NSURL?
    var drawingView: DrawingView = {
        let view = DrawingView()
        return view
    }()
    var timeLimit: Int = 5
    
    var isInDrawingMode: Bool = false
    
    let activityIndicatorView: UIActivityIndicatorView = {
        let aiv = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        aiv.hidesWhenStopped = true
        return aiv
    }()
    
    lazy var backGroundImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    lazy var cancelButton: UIButton = {
        let button = UIButton(type: UIButtonType.system)
        button.setTitle("X", for: UIControlState.normal)
        button.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor(r: 21, g: 139, b: 201), for: UIControlState.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        button.addTarget(self, action: #selector(handleReturn), for: UIControlEvents.touchUpInside)
        return button
    }()
    
    lazy var mapButton: UIButton = {
        let button = UIButton(type: UIButtonType.system)
        button.setTitle("L", for: UIControlState.normal)
        button.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor(r: 21, g: 139, b: 201), for: UIControlState.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        button.addTarget(self, action: #selector(handleLoadMap), for: UIControlEvents.touchUpInside)
        return button
    }()
    
    lazy var textButton: UIButton = {
        let button = UIButton()
        button.setTitle("T", for: UIControlState.normal)
        button.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor(r: 21, g: 139, b: 201), for: UIControlState.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        button.addTarget(self, action: #selector(handleShowText), for: UIControlEvents.touchUpInside)
        return button
    }()
    
    lazy var pictogramsButton: UIButton = {
        let button = UIButton()
        button.setTitle("P", for: UIControlState.normal)
        button.addTarget(self, action: #selector(handleShowPictograms), for: UIControlEvents.touchUpInside)
        button.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor(r: 21, g: 139, b: 201), for: UIControlState.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        return button
    }()
    
    lazy var drawButton: UIButton = {
        let button = UIButton()
        button.setTitle("D", for: UIControlState.normal)
        button.addTarget(self, action: #selector(handleShowDraw), for: UIControlEvents.touchUpInside)
        button.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor(r: 21, g: 139, b: 201), for: UIControlState.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        return button
    }()
    
    lazy var storyButton: UIButton = {
        let button = UIButton()
        button.setTitle("S", for: UIControlState.normal)
        button.addTarget(self, action: #selector(handleSaveStory), for: UIControlEvents.touchUpInside)
        button.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor(r: 21, g: 139, b: 201), for: UIControlState.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        return button
    }()
    
    lazy var memoryButton: UIButton = {
        let button = UIButton()
        button.setTitle("M", for: UIControlState.normal)
        button.addTarget(self, action: #selector(handleSaveMemory), for: UIControlEvents.touchUpInside)
        button.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor(r: 21, g: 139, b: 201), for: UIControlState.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        return button
    }()
    
    lazy var shareButton: UIButton = {
        let button = UIButton()
        button.setTitle("Share", for: UIControlState.normal)
        button.addTarget(self, action: #selector(handleShare), for: UIControlEvents.touchUpInside)
        button.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor(r: 21, g: 139, b: 201), for: UIControlState.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        return button
    }()
    
    lazy var removeButton: UIButton = {
        let button = UIButton()
        button.setTitle("X", for: UIControlState.normal)
        button.addTarget(self, action: #selector(handleRemove), for: UIControlEvents.touchUpInside)
        button.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor(r: 21, g: 139, b: 201), for: UIControlState.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        return button
    }()
    
    lazy var timerButton: UIButton = {
        let button = UIButton()
        button.setTitle("T", for: UIControlState.normal)
        button.addTarget(self, action: #selector(handleSetTimer), for: UIControlEvents.touchUpInside)
        button.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor(r: 21, g: 139, b: 201), for: UIControlState.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupGeneralAspects()
    }
    
    func setupGeneralAspects() {
        
        view.backgroundColor = UIColor.black
        
//        if let imageWidth = image?.size.width, let imageHeight = image?.size.height {
//            let imageDisplayHeight = view.bounds.width * imageHeight / imageWidth
//            backGroundImageView.frame = CGRect(x: 0, y: (view.bounds.height - imageDisplayHeight) / 2 , width: view.bounds.width, height: imageDisplayHeight)
//            backGroundImageView.image = image
//        } else {
//            backGroundImageView.frame = view.bounds
//            backGroundImageView.image = image
//        }
        
        backGroundImageView.frame = view.bounds
        backGroundImageView.image = image
        backGroundImageView.backgroundColor = UIColor.black
        
        drawingView.frame = backGroundImageView.bounds
        drawingView.backgroundColor = UIColor.clear
        
        mapButton.frame = CGRect(x: 80, y: 10, width: 50, height: 50)
        cancelButton.frame = CGRect(x: 20, y: 10, width: 50, height: 50)
        pictogramsButton.frame = CGRect(x: view.bounds.width - 190, y: 10, width: 50, height: 50)
        textButton.frame = CGRect(x: view.bounds.width - 130, y: 10, width: 50, height: 50)
        drawButton.frame = CGRect(x: view.bounds.width - 70, y: 10, width: 50, height: 50)
        removeButton.frame = CGRect(x: view.bounds.width - 130, y: 10, width: 50, height: 50)
        shareButton.frame = CGRect(x: view.bounds.width - 100, y: view.bounds.height - 60, width: 80, height: 50)
        storyButton.frame = CGRect(x: 140, y: view.bounds.height - 60, width: 50, height: 50)
        memoryButton.frame = CGRect(x: 80, y: view.bounds.height - 60, width: 50, height: 50)
        timerButton.frame = CGRect(x: 20, y: view.bounds.height - 60, width: 50, height: 50)
        activityIndicatorView.frame = CGRect(x: view.bounds.width/2 - 20, y: view.bounds.height/2 - 20, width: 40, height: 40)
        
        view.addSubview(backGroundImageView)
        backGroundImageView.addSubview(drawingView)
        view.addSubview(cancelButton)
        view.addSubview(mapButton)
        view.addSubview(pictogramsButton)
        view.addSubview(textButton)
        view.addSubview(shareButton)
        view.addSubview(drawButton)
        view.addSubview(removeButton)
        view.addSubview(storyButton)
        view.addSubview(memoryButton)
        view.addSubview(timerButton)
        view.addSubview(activityIndicatorView)
        removeButton.isHidden = true
        
    }
    
    func handleRemove() {
        let _ = drawingView.paths.popLast()
    }
    
    func handleLoadMap() {
        let storyBoard = UIStoryboard(name: "Memories", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        controller.editingController = self
        present(controller, animated: false, completion: nil)
    }
    
    func handleSetTimer() {
        let storyboard = UIStoryboard(name: "Memories", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TimerController") as! TimerController
        vc.modalPresentationStyle = UIModalPresentationStyle.popover
        vc.editingController = self
        vc.preferredContentSize = CGSize(width: view.bounds.width, height: view.bounds.height/2)
        if let presentationController = vc.popoverPresentationController {
            presentationController.delegate = self
            presentationController.sourceView = timerButton
            present(vc, animated: true, completion: nil)
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func handleShowText() {
        let textfield = UITextField()
        let recognizer = UIPanGestureRecognizer(target: self, action: #selector(handleMoveUpAndDown(recognizer:)))
        textfield.addGestureRecognizer(recognizer)
        textfield.frame = CGRect(x: 0, y: view.bounds.height/2 - 50, width: view.bounds.width, height: 50)
        textfield.font = UIFont.boldSystemFont(ofSize: 30)
        textfield.textColor = UIColor.white
        textfield.backgroundColor = UIColor.gray
        textfield.layer.opacity = 0.6
        textfield.delegate = self
        backGroundImageView.addSubview(textfield)
    }
    
    func handleShowDraw() {
        isInDrawingMode = !isInDrawingMode
        if (isInDrawingMode) {
            let recognizer = UIPanGestureRecognizer(target: self, action: #selector(handleDraw(recognizer:)))
            drawingView.addGestureRecognizer(recognizer)
            pictogramsButton.isHidden = true
            textButton.isHidden = true
            removeButton.isHidden = false
        } else {
            drawingView.gestureRecognizers?.removeAll()
            pictogramsButton.isHidden = false
            textButton.isHidden = false
            removeButton.isHidden = true
        }
    }
    
    func handleDraw(recognizer: UIPanGestureRecognizer) {
        let point = recognizer.location(in: backGroundImageView)
        
        switch recognizer.state {
        case .began:
            drawingView.currentPath = UIBezierPath()
            drawingView.currentPath?.lineWidth = 5
            drawingView.currentPath?.move(to: point)
        case .changed:
            let translation = recognizer.translation(in: backGroundImageView)
            recognizer.setTranslation(CGPoint.zero, in: backGroundImageView)
            drawingView.currentPath?.addLine(to: CGPoint(x: point.x + translation.x, y: point.y + translation.y))
            drawingView.setNeedsDisplay()
        case .ended:
            if let path = drawingView.currentPath {
                drawingView.paths.append(path)
            }
            drawingView.currentPath = nil
        default: break
        }
        
    }
    
    func handleReturn() {
        redirectDelegate?.switchToCameraController()
    }
    
    func handleShowPictograms(_ sender: AnyObject) {
        let controller = PictogramsController()
        controller.editingController = self
        present(controller, animated: true, completion: nil)
    }
    
    func handlePictogram(view: UIImageView)
    {
        let recognizer = UIPanGestureRecognizer(target: self, action: #selector(handleMoveAround(recognizer:)))
        view.addGestureRecognizer(recognizer)
        backGroundImageView.addSubview(view)
    }
    
    func handleMoveAround(recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .changed: fallthrough
        case .ended:
            let translation = recognizer.translation(in: backGroundImageView)
            recognizer.setTranslation(CGPoint.zero, in: backGroundImageView)
            if let targetView = recognizer.view {
                targetView.frame.origin = CGPoint(x: targetView.frame.origin.x + translation.x, y: targetView.frame.origin.y + translation.y)
            }
        default: break
        }
        
    }
    
    func handleMoveUpAndDown(recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .changed: fallthrough
        case .ended:
            let translation = recognizer.translation(in: backGroundImageView)
            recognizer.setTranslation(CGPoint.zero, in: backGroundImageView)
            if let targetView = recognizer.view {
                targetView.frame.origin = CGPoint(x: targetView.frame.origin.x, y: targetView.frame.origin.y + translation.y)
            }
        default: break
        }
        
    }
    
    func handleShare() {
        let renderer = UIGraphicsImageRenderer(size: view.bounds.size)
        let image = renderer.image { (ctx) in
            backGroundImageView.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        }
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.leftController?.handleNewMessageWithImage(image: image, timeLimit: timeLimit)
        }
    }
    
    func handleSaveStory() {
        let renderer = UIGraphicsImageRenderer(size: view.bounds.size)
        let image = renderer.image { (ctx) in
            backGroundImageView.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        }
        uploadToFirebaseStorageUsingImageForStory(image: image)
    }
    
    func handleSaveMemory() {
        let renderer = UIGraphicsImageRenderer(size: view.bounds.size)
        let image = renderer.image { (ctx) in
            backGroundImageView.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        }
        uploadToFirebaseStorageUsingImageForMemory(image: image)
    }
    
    func uploadToFirebaseStorageUsingImageForStory(image: UIImage){
        activityIndicatorView.startAnimating()
        let imageName = NSUUID().uuidString
        let storageRef = FIRStorage.storage().reference().child("story_images").child("\(imageName).jpg")
        
        // can also use "uploadData = UIImagePNGRepresentation(self.profileImageView.image!)" but the quality will be high
        
        if let uploadData = UIImageJPEGRepresentation(image , 0.2){
            storageRef.put(uploadData, metadata: nil, completion: { (metadata, error) in
                if error != nil {
                    return
                }
                
                if let imageUrl = metadata?.downloadURL()?.absoluteString {
                    self.sendStoryWithImageUrl(imageUrl: imageUrl, image: image)
                }
            })
        }
    }
    
    private func sendStoryWithImageUrl(imageUrl: String, image: UIImage) {
        let ref = FIRDatabase.database().reference().child("stories")
        let childRef = ref.childByAutoId()
        
        if let uid = FIRAuth.auth()?.currentUser?.uid {
            let timestamp = Int(Date().timeIntervalSince1970)
            let values = ["imageUrl": imageUrl, "uid": uid, "timestamp": timestamp, "imageWidth": image.size.width, "imageHeight": image.size.height] as [String : Any]
            
            childRef.updateChildValues(values) { (error, ref) in
                if error != nil {
                    return
                }
                
                let storyId = childRef.key
                
                let userStoriesRef = FIRDatabase.database().reference().child("user-stories").child(uid)
                userStoriesRef.updateChildValues([storyId: 1])
                self.activityIndicatorView.stopAnimating()
            }
        }
        
    }
    
    func uploadToFirebaseStorageUsingImageForMemory(image: UIImage){
        activityIndicatorView.startAnimating()
        let imageName = NSUUID().uuidString
        let storageRef = FIRStorage.storage().reference().child("memory_images").child("\(imageName).jpg")
        
        // can also use "uploadData = UIImagePNGRepresentation(self.profileImageView.image!)" but the quality will be high
        
        if let uploadData = UIImageJPEGRepresentation(image , 0.2){
            storageRef.put(uploadData, metadata: nil, completion: { (metadata, error) in
                if error != nil {
                    return
                }
                
                if let imageUrl = metadata?.downloadURL()?.absoluteString {
                    self.sendMemoryWithImageUrl(imageUrl: imageUrl, image: image)
                }
            })
        }
    }
    
    private func sendMemoryWithImageUrl(imageUrl: String, image: UIImage) {
        let ref = FIRDatabase.database().reference().child("memories")
        let childRef = ref.childByAutoId()
        
        if let uid = FIRAuth.auth()?.currentUser?.uid {
            let timestamp = Int(Date().timeIntervalSince1970)
            let values = ["imageUrl": imageUrl, "uid": uid, "timestamp": timestamp, "imageWidth": image.size.width, "imageHeight": image.size.height] as [String : Any]
            
            childRef.updateChildValues(values) { (error, ref) in
                if error != nil {
                    return
                }
                
                let memoryId = childRef.key
                
                let userStoriesRef = FIRDatabase.database().reference().child("user-memories").child(uid)
                userStoriesRef.updateChildValues([memoryId: 1])
                self.activityIndicatorView.stopAnimating()
            }
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.backgroundColor = UIColor.gray
        textField.layer.opacity = 0.6
        textField.textAlignment = NSTextAlignment.left
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        textField.backgroundColor = UIColor.clear
        textField.layer.opacity = 1
        textField.textAlignment = NSTextAlignment.center
    }

}

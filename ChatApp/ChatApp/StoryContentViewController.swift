//
//  StoryContentViewController.swift
//  ChatStories
//
//  Created by Liu Sam on 10/5/16.
//  Copyright © 2016 Liu Sam. All rights reserved.
//

import UIKit
import Firebase

class StoryContentViewController: UIViewController {
    
    
    
    private var _pageIndex: Int!
    private var _media: String!
    private var _content: String!
    private var _title: String!
    private var _userId: String!
    private var _isPublic: Bool!
    
    
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet weak var footerView: UIView!
    
    @IBOutlet weak var subscribeButton: UIButton!
    
    var imageView: UIImageView!
    private var contentWebView: UIWebView!

    
    var pageIndex: Int {
        return _pageIndex!
    }
    
    var isPublic: Bool {
        set {
            _isPublic = newValue
        }
        
        get {
            return _isPublic
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // observe subscribe info
        if let uid = FIRAuth.auth()?.currentUser?.uid {
            DataService.ds.REF_SUBSCRIBE.child(uid).observe(.value, with: { (snapshot) in
                if snapshot.hasChild(self._userId) {
                    self.subscribeButton.setTitle("Subscribed", for: UIControlState.normal)
                    self.subscribeButton.setImage(#imageLiteral(resourceName: "subscribe_check"), for: UIControlState.normal)
                } else {
                    self.subscribeButton.setTitle("Subscribe", for: UIControlState.normal)
                    self.subscribeButton.setImage(#imageLiteral(resourceName: "subscribe_flag"), for: UIControlState.normal)
                    
                }
            })
        }
        
        let imagelabel = UILabel(frame: CGRect(x: 10, y: 30, width: view.frame.size.width, height: 20))
        imagelabel.textColor = UIColor.white
        imagelabel.text = _title
        
        imageView = UIImageView(frame: view.frame)
        imageView.backgroundColor = UIColor.white
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        imageView.addSubview(imagelabel)
        imageView.loadImageUsingCacheWithUrlString(urlString: _media)

        contentWebView = UIWebView(frame: CGRect(x:0, y:view.bounds.size.height, width: view.frame.size.width, height: 1))
        contentWebView.delegate = self
        
        scrollView.addSubview(imageView)
        scrollView.addSubview(contentWebView)

        
        view.alpha = 0.1
        contentWebView.loadHTMLString(_content, baseURL: nil)
        UIView.animate(withDuration: 1.0, animations: { () -> Void in
            self.view.alpha = 1.0
        })
        
        scrollView.autoresizingMask = UIViewAutoresizing.flexibleHeight
        scrollView.delegate = self
        
        if _isPublic == false {
            footerView.alpha = 0
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func applyData(pageIndex: Int, storyData: Story, userID: String) {
        _pageIndex = pageIndex
        _content = storyData.content
        _media = storyData.media
        _title = storyData.title
        _userId = userID
    }
    

    @IBAction func subscribeAction(_ sender: UIButton) {
        if let uid = FIRAuth.auth()?.currentUser?.uid {
            if sender.titleLabel?.text == "Subscribed" {
                DataService.ds.REF_SUBSCRIBE.child(uid).child(_userId).removeValue()
            } else {
                DataService.ds.REF_SUBSCRIBE.child(uid).child(_userId).setValue(true)
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension StoryContentViewController: UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        webView.frame.size.height = 1
        webView.frame.size = webView.sizeThatFits(CGSize.zero)
        self.scrollView.contentSize = CGSize(width: view.bounds.width, height: self.imageView.bounds.height + webView.bounds.height + footerView.frame.height)
        let frame = CGRect(x: 0, y: self.imageView.bounds.height + webView.bounds.height, width: view.bounds.width, height: 65)
        footerView.frame = frame
    }
}


extension StoryContentViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollOffset = scrollView.contentOffset.y
        
        if scrollOffset == 0 {
            dismiss(animated: true, completion: nil)
        }
    }
}

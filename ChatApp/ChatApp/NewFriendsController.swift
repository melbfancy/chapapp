//
//  NewFriendsController.swift
//  ChatApp
//
//  Created by Fan Qi on 16/10/7.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import UIKit
import Firebase

class NewFriendsController: UIViewController {
    
    lazy var barcodeImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var addFriendsImageView: UIImageView = {
        let image = UIImage(named: "new_friend_icon")
        let imageView = UIImageView()
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        imageView.image = image
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var addFriendsButton: UIButton = {
        let button = UIButton(type: UIButtonType.system)
        button.setTitle("Add Friends", for: UIControlState.normal)
        button.addTarget(self, action: #selector(handleAddFriend), for: UIControlEvents.touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        return button
    }()
    
    lazy var addedMeImageView: UIImageView = {
        let image = UIImage(named: "added_me_icon")
        let imageView = UIImageView()
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        imageView.image = image
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var addedMeButton: UIButton = {
        let button = UIButton(type: UIButtonType.system)
        button.setTitle("Added me", for: UIControlState.normal)
        button.addTarget(self, action: #selector(handleAddedMe), for: UIControlEvents.touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        return button
    }()
    
    lazy var myFriendsImageView: UIImageView = {
        let image = UIImage(named: "my_friends_icon")
        let imageView = UIImageView()
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        imageView.image = image
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var myFriendsButton: UIButton = {
        let button = UIButton(type: UIButtonType.system)
        button.setTitle("My friends", for: UIControlState.normal)
        button.addTarget(self, action: #selector(handleMyFriends), for: UIControlEvents.touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        return button
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        setupGeneralAspects()
        setupUserSpecificAspects()
    }
    
    func handleAddFriend(){
        navigationController?.pushViewController(MethodsController(), animated: true)
    }
    
    func handleAddedMe() {
        navigationController?.pushViewController(RequestsController(), animated: true)
    }
    
    func handleMyFriends() {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.leftController?.handleNewMessage()
        }
    }
    
    func setupGeneralAspects() {
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: UIBarButtonItemStyle.plain, target: self, action: #selector(handleLogout))
        view.backgroundColor = UIColor(r: 243, g: 243, b: 155)
        view.addSubview(addedMeImageView)
        view.addSubview(addedMeButton)
        view.addSubview(addFriendsImageView)
        view.addSubview(addFriendsButton)
        view.addSubview(myFriendsImageView)
        view.addSubview(myFriendsButton)
        view.addSubview(barcodeImageView)
        
        addedMeImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -70).isActive = true
        addedMeImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 60).isActive = true
        addedMeImageView.heightAnchor.constraint(equalToConstant: 32).isActive = true
        addedMeImageView.widthAnchor.constraint(equalToConstant: 32).isActive = true
        
        addedMeButton.leftAnchor.constraint(equalTo: addedMeImageView.rightAnchor, constant: 20).isActive = true
        addedMeButton.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 60).isActive = true
        addedMeButton.heightAnchor.constraint(equalToConstant: 32).isActive = true
        
        addFriendsImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -70).isActive = true
        addFriendsImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 120).isActive = true
        addFriendsImageView.heightAnchor.constraint(equalToConstant: 32).isActive = true
        addFriendsImageView.widthAnchor.constraint(equalToConstant: 32).isActive = true
        
        addFriendsButton.leftAnchor.constraint(equalTo: addFriendsImageView.rightAnchor, constant: 20).isActive = true
        addFriendsButton.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 120).isActive = true
        addFriendsButton.heightAnchor.constraint(equalTo: addFriendsImageView.heightAnchor).isActive = true
        
        myFriendsImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -70).isActive = true
        myFriendsImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 180).isActive = true
        myFriendsImageView.heightAnchor.constraint(equalToConstant: 32).isActive = true
        myFriendsImageView.widthAnchor.constraint(equalToConstant: 32).isActive = true
        
        myFriendsButton.leftAnchor.constraint(equalTo: addedMeImageView.rightAnchor, constant: 20).isActive = true
        myFriendsButton.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 180).isActive = true
        myFriendsButton.heightAnchor.constraint(equalToConstant: 32).isActive = true
        
        barcodeImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        barcodeImageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 100).isActive = true
        barcodeImageView.widthAnchor.constraint(equalToConstant: 180).isActive = true
        barcodeImageView.heightAnchor.constraint(equalToConstant: 180).isActive = true
        
    }
    
    func setupUserSpecificAspects() {
        barcodeImageView.image = nil
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            if let user = appDelegate.currentUser {
                navigationItem.title = user.name
                if let barcodeImageUrl = user.barcodeImageUrl
                {
                    barcodeImageView.loadImageUsingCacheWithUrlString(urlString: barcodeImageUrl)
                }
            }
        }

    }
    
    func handleLogout(){
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.snapchatCrossPagerViewController?.handleLogout()
        }
    }
    
}

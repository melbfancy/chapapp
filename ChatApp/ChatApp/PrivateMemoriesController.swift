//
//  PrivateMemoriesController.swift
//  ChatApp
//
//  Created by Fan Qi on 16/10/17.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import UIKit

class PrivateMemoriesController: UIViewController, UserSpecificSetupProtocol {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUserSpecificAspects()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupUserSpecificAspects() {
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

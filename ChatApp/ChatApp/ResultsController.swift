//
//  ResultsController.swift
//  ChatApp
//
//  Created by Fan Qi on 16/10/8.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import UIKit
import Firebase

class ResultsController: UITableViewController, UISearchResultsUpdating, FriendCellButtonProtocol {
    
    var method: Method?
    
    var users = [User]()
    
    var filteredUsers = [User]()
    
    var searchText: String? {
        didSet {
            filterUsers()
            tableView.reloadData()
        }
    }
    
    let searchController = UISearchController(searchResultsController: nil)

    override func viewDidLoad() {
        super.viewDidLoad()
    
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        navigationItem.title = method?.text
        tableView.tableHeaderView = searchController.searchBar
        
        tableView.register(FriendCell.self, forCellReuseIdentifier: "reuseIdentifier")
        tableView.allowsSelection = false
        fetchUsers()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    func fetchUsers() {
        let ref = FIRDatabase.database().reference()
        ref.child("users").observe(FIRDataEventType.childAdded, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let user = User()
                user.id = snapshot.key
                // a convinient way to set all the properties
                // if unmatched, will crash
                user.setValuesForKeys(dictionary)
                self.users.append(user)
            }
        })
    }
    
    func filterUsers() {
        filteredUsers = users.filter { (user) -> Bool in
            if searchText == nil || searchText == "" {
                return false
            } else {
                return user.email!.lowercased().contains(searchText!.lowercased())
            }
        }
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        searchText = searchController.searchBar.text!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredUsers.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! FriendCell
        
        // Configure the cell...
        let user = filteredUsers[indexPath.row]
        if user.id == FIRAuth.auth()?.currentUser?.uid {
            cell.user = user
            cell.buttonDelegate = nil
            cell.setupButton(title: "Myself")
            cell.setupCell()
        } else {
            if let uid = FIRAuth.auth()?.currentUser?.uid, let toId = user.id {
                let ref = FIRDatabase.database().reference().child("relations").child(uid).child(toId)
                ref.observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                    if let _ = snapshot.value as? Bool {
                        cell.user = user
                        cell.buttonDelegate = nil
                        cell.setupButton(title: "Added")
                        cell.setupCell()
                    } else {
                        cell.user = user
                        cell.buttonDelegate = self
                        cell.setupButton(title: "Add")
                        cell.setupCell()
                    }
                })
            }
        }
        return cell
    }
    
    func performOperation(user: User) {
        let ref = FIRDatabase.database().reference().child("requests")
        let childRef = ref.childByAutoId()
        
        if let toId = user.id, let fromId = FIRAuth.auth()?.currentUser?.uid {
            let timestamp = Int(Date().timeIntervalSince1970)
            let values = ["toId": toId, "fromId": fromId, "timestamp": timestamp] as [String : Any]
            
            childRef.updateChildValues(values) { (error, ref) in
                if error != nil {
                    return
                }
                
                let requestId = childRef.key
                
                let userRequestsRef = FIRDatabase.database().reference().child("user-requests").child(toId).child(fromId)
                userRequestsRef.updateChildValues([requestId: 1])
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            }
        }

    }

}

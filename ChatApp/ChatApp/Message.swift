//
//  Message.swift
//  ChatApp
//
//  Created by Fan Qi on 16/9/29.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import Foundation
import Firebase

class Message: NSObject {
    
    var id: String?
    var fromId: String?
    var text: String?
    var toId: String?
    var timestamp: NSNumber?
    var imageUrl: String?
    var imageHeight: NSNumber?
    var imageWidth: NSNumber?
    var videoUrl: String?
    var readCount: Int?
    var timeLimit: Int?
    
    func chatPartnerId() -> String? {
        return fromId == FIRAuth.auth()?.currentUser?.uid ? toId : fromId
    }
    
    init(dictionary: [String: AnyObject]) {
        super.init()
        
        fromId = dictionary["fromId"] as? String
        text = dictionary["text"] as? String
        timestamp = dictionary["timestamp"] as? NSNumber
        toId = dictionary["toId"] as? String
        
        imageUrl = dictionary["imageUrl"] as? String
        imageHeight = dictionary["imageHeight"] as? NSNumber
        imageWidth = dictionary["imageWidth"] as? NSNumber
        videoUrl = dictionary["videoUrl"] as? String
        timeLimit = dictionary["timeLimit"] as? Int
        readCount = dictionary["readCount"] as? Int
    }
}

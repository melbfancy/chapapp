//
//  DiscoverView.swift
//  ChatStories
//
//  Created by Liu Sam on 10/1/16.
//  Copyright © 2016 Liu Sam. All rights reserved.
//

import UIKit

class DiscoverView: UIView {
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    private var navBar: UINavigationBar!
    private var backButton: UIBarButtonItem!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupUI()
        self.setupConstraints()
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        navBar = UINavigationBar()
        navBar!.tintColor = UIColor.purple
        self.addSubview(navBar!);
        
        let navItem = UINavigationItem(title: "Discover");
        
        backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "camera_icon"), landscapeImagePhone: nil, style: UIBarButtonItemStyle.plain, target: self, action: #selector(buttonPress(button:)))
        
        navItem.leftBarButtonItem = backButton
        
        navBar!.setItems([navItem], animated: false)
    }
    
    private func setupConstraints() {
        // layout navbar
        navBar!.translatesAutoresizingMaskIntoConstraints = false
        navBar!.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        navBar!.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        //navBar!.topAnchor.constraint(equalTo: view.topAnchor, constant: 30.0).isActive = true
        navBar!.heightAnchor.constraint(equalToConstant: 70.0).isActive = true
        
        
    }
    
    func buttonPress(button:AnyObject) {
        //delegate?.buttonPressedAction(button: button)
    }
    
}

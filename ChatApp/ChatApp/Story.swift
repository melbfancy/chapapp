  //
//  Story.swift
//  ChatStories
//
//  Created by Liu Sam on 10/3/16.
//  Copyright © 2016 Liu Sam. All rights reserved.
//

import Foundation

class Stories {
    private var _user: User?
    private var _stories = [Story]()
    private var _rating = 0
 
    var user: User? {
        return _user
    }
    
    var size: Int {
        return _stories.count
    }
    
    var rating: Int {
        get {
            return _rating
        }
        
        set {
            _rating = newValue
        }
    }
    
    func story(at index:Int) -> Story? {
        if _stories.indices.contains(index){
            return _stories[index]
        } else {
            return nil
        }
    }
    
    func addStory(story: Story) {
        _stories.append(story)
    }
    
    init(user: User, storiesData: [Story]) {
        _user = user
        _stories = storiesData
    }
}

class Story {
    private var _title: String!
    private var _thumbnail: String!
    private var _media: String!
    private var _content: String!
    private var _storyId: String!
    private var _timestamp: NSNumber!
    
    var title: String {
        get {
            return _title
        }
        
        set {
            _title = newValue
        }
    }
    
    var thumbnail: String {
        return _thumbnail
    }
    
    var media: String {
        return _media
    }
    
    var content: String{
        return _content
    }
    
    var storyId: String {
        return _storyId
    }
    
    var timestamp: NSNumber {
        return _timestamp
    }
    
    // user story initialiser
    init(storyId: String, imageUrl: String, timestamp: NSNumber) {
        _storyId = storyId
        _media = imageUrl
        _timestamp = timestamp
        _content = ""
    }

    
    init(storyId: String, storyData: Dictionary<String, AnyObject>) {
        _storyId = storyId
        
        if let title = storyData["title"] as? String {
            _title = title
        } else {
            _title = ""
        }
        
        if let content = storyData["content"] as? String {
            _content = content
        } else {
            _content = ""
        }
        
        if let thumbnail = storyData["imageUrl"] as? String {
            _thumbnail = thumbnail
        } else {
            _thumbnail = ""
        }
        
        if let media = storyData["media"] as? String {
            _media = media
        } else {
            _media = ""
        }
        
        if let timestamp = storyData["timestamp"] as? NSNumber {
            _timestamp = timestamp
        } else {
            _timestamp = 0
        }

    }
    
}

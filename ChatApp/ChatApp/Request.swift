//
//  Request.swift
//  ChatApp
//
//  Created by Fan Qi on 16/10/8.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import Foundation
import UIKit

class Request: NSObject {
    
    var fromId: String?
    var toId: String?
    var timestamp: NSNumber?
    var approved: Bool?
    
}

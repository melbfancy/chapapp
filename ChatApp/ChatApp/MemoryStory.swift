//
//  MemoryStory.swift
//  ChatApp
//
//  Created by Fan Qi on 16/10/17.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import Foundation

class MemoryStory: NSObject {
    
    var id: String?
    var dict: [String: AnyObject]?
    
}

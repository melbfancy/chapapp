//
//  ImageDisplayController.swift
//  ChatApp
//
//  Created by Fan Qi on 16/10/15.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import UIKit
import Firebase

class ImageDisplayController: UIViewController, UIScrollViewDelegate {
    
    var imageToDisplay: UIImage?
    
    var chatLogController: ChatLogController?
    
    var count: Int?
    
    var timer: Timer?
    
    var message: Message?
    
    var imageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    var timeLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor(r: 21, g: 139, b: 201)
        label.layer.cornerRadius = 20
        label.layer.masksToBounds = true
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        return label
    }()

    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.delegate = self
            scrollView.minimumZoomScale = 1.0
            scrollView.maximumZoomScale = 2.0
            scrollView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        count = message?.timeLimit
        // Do any additional setup after loading the view.
        scrollView.frame = view.bounds
        scrollView.backgroundColor = UIColor.black
        scrollView.contentSize = view.bounds.size
        scrollView.contentOffset = CGPoint(x: 0, y: 0)
        imageView.frame = scrollView.bounds
        imageView.image = imageToDisplay
        timeLabel.frame = CGRect(x: 20, y: 20, width: 40, height: 40)
        timeLabel.text = count?.description
        scrollView.addSubview(imageView)
        scrollView.addSubview(timeLabel)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        if count != nil {
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(handleCount), userInfo: nil, repeats: true)
        }
    }
    
    func handleCount() {
        if count! >= 0 {
            DispatchQueue.main.async {
                self.timeLabel.text = self.count?.description
                self.count! = self.count! - 1
            }
        } else {
            timer?.invalidate()
            dismiss(animated: false, completion: {
                if let readCount = self.message?.readCount, let id = self.message?.id {
                    if readCount == 2 {
                        self.message?.readCount = 1
                        let ref = FIRDatabase.database().reference().child("messages").child(id)
                        ref.updateChildValues(["readCount" : 1])
                    } else if readCount == 1 {
                        if let fromId = self.message?.fromId, let toId = self.message?.toId, let messageId = self.message?.id {
                            let userMessagesRef = FIRDatabase.database().reference().child("user-messages").child(fromId).child(toId)
                            userMessagesRef.child(messageId).removeValue()
                            let recipientUserMessagesRef = FIRDatabase.database().reference().child("user-messages").child(toId).child(fromId)
                            recipientUserMessagesRef.child(messageId).removeValue()
                        }
                    }
                }
            })
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func handleTap() {
        dismiss(animated: false, completion: {
            if let readCount = self.message?.readCount, let id = self.message?.id {
                if readCount == 2 {
                    self.message?.readCount = 1
                    let ref = FIRDatabase.database().reference().child("messages").child(id)
                    ref.updateChildValues(["readCount" : 1])
                } else if readCount == 1 {
                    if let fromId = self.message?.fromId, let toId = self.message?.toId, let messageId = self.message?.id {
                        let userMessagesRef = FIRDatabase.database().reference().child("user-messages").child(fromId).child(toId)
                        userMessagesRef.child(messageId).removeValue()
                        let recipientUserMessagesRef = FIRDatabase.database().reference().child("user-messages").child(toId).child(fromId)
                        recipientUserMessagesRef.child(messageId).removeValue()
                    }
                }
            }
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

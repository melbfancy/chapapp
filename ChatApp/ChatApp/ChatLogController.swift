//
//  ChatLogController.swift
//  ChatApp
//
//  Created by Fan Qi on 16/9/29.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import UIKit

import UIKit
import Firebase
import MobileCoreServices
import AVFoundation

class ChatLogController: UICollectionViewController, UITextFieldDelegate, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var timer: Timer?
    
    var user: User? {
        didSet {
            navigationItem.title = user?.name
            observeMessages()
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    var messages = [Message]()
    
    let cellId = "cellId"
    
    var containerViewBottomAnchor: NSLayoutConstraint?
    
    lazy var inputTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Enter message..."
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.delegate = self
        return textField
    }()
    
    let activityIndicatorView: UIActivityIndicatorView = {
        let aiv = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        aiv.translatesAutoresizingMaskIntoConstraints = false
        aiv.hidesWhenStopped = true
        return aiv
    }()
    
    func observeMessages() {
        guard let uid = FIRAuth.auth()?.currentUser?.uid, let toId = user?.id else {
            return
        }
        
        let userMessagesRef = FIRDatabase.database().reference().child("user-messages").child(uid).child(toId)
        userMessagesRef.observe(FIRDataEventType.childAdded, with: { (snapshot) in
            let messageId = snapshot.key
            let messagesRef = FIRDatabase.database().reference().child("messages").child(messageId)
            messagesRef.observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                guard let dictionary = snapshot.value as? [String: AnyObject] else {
                    return
                }
                
                let message = Message(dictionary: dictionary)
                message.id = messageId
                // potential of crashing if keys don't match
                // another convinient way of setting properties
                // message.setValuesForKeys(dictionary)
                
                self.messages.append(message)
                self.attemptReloadOfTable()
            })
        })
        userMessagesRef.observe(FIRDataEventType.childRemoved, with: { (snapshot) in
            self.messages = self.messages.filter({ (message) -> Bool in
                return message.id != snapshot.key
            })
            self.attemptReloadOfTable()
        })
    }
    
    private func attemptReloadOfTable() {
        self.timer?.invalidate()
        
        self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.handleReloadTable), userInfo: nil, repeats: false)
    }
    
    func handleReloadTable() {
        DispatchQueue.main.async {
            self.collectionView?.reloadData()
            //scroll to the last index
            if self.messages.count > 0 {
                let indexPath = IndexPath(row: self.messages.count - 1, section: 0)
                self.collectionView?.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.bottom, animated: false)
            }
        }
    }
    
    func setupKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardDidShow), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    func handleKeyboardDidShow(notification: NSNotification) {
        if self.messages.count > 0 {
            let indexPath = IndexPath(row: self.messages.count - 1, section: 0)
            self.collectionView?.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.top, animated: true)
        }
    }
    
    func handleKeyboardWillShow(notification: NSNotification) {
        
        if let keyboardFrame = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue, let keyboardDuration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? Double {
            containerViewBottomAnchor?.constant = -keyboardFrame.cgRectValue.height
            UIView.animate(withDuration: keyboardDuration, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func handleKeyboardWillHide(notification: NSNotification) {
        if let keyboardDuration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? Double {
            containerViewBottomAnchor?.constant = 0
            UIView.animate(withDuration: keyboardDuration) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(handleCancel))
        
        collectionView?.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 58, right: 0)
        collectionView?.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
        collectionView?.alwaysBounceVertical = true
        collectionView?.register(ChatMessageCell.self, forCellWithReuseIdentifier: cellId)
        collectionView?.backgroundColor = UIColor.white
        
        view.addSubview(activityIndicatorView)
        
        activityIndicatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        activityIndicatorView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        activityIndicatorView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        activityIndicatorView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        setupInputComponents()
        setupKeyboardObservers()
    }
    
    func handleCancel(){
        dismiss(animated: true, completion: nil)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ChatMessageCell
        let message = messages[indexPath.item]
        setupCell(cell: cell, message: message)
        return cell
    }
    
    private func setupCell(cell: ChatMessageCell, message: Message) {
        
        cell.chatLogController = self
        cell.message = message
        
        if message.fromId == FIRAuth.auth()?.currentUser?.uid {
            //outgoing blue
            cell.bubbleView.backgroundColor = ChatMessageCell.blueColor
            cell.textView.textColor = UIColor.white
            cell.profileImageView.isHidden = true
            
            cell.bubbleViewRightAnchor?.isActive  = true
            cell.bubbleViewLeftAnchor?.isActive = false
            
        } else {
            //incoming gray
            cell.bubbleView.backgroundColor = UIColor(r: 240, g: 240, b: 240)
            cell.textView.textColor = UIColor.black
            cell.profileImageView.isHidden = false
            
            if let profileImageUrl = self.user?.profileImageUrl {
                cell.profileImageView.loadImageUsingCacheWithUrlString(urlString: profileImageUrl)
            }
            
            cell.bubbleViewRightAnchor?.isActive = false
            cell.bubbleViewLeftAnchor?.isActive = true
        }
        
        if let messageImageUrl = message.imageUrl {
            cell.messageImageView.loadImageUsingCacheWithUrlString(urlString: messageImageUrl)
            cell.messageImageView.isHidden = false
            cell.textView.isHidden = true
            cell.bubbleView.backgroundColor = UIColor.clear
            cell.bubbleWidthAnchor?.constant = 200
        } else if let text = message.text{
            cell.textView.isHidden = false
            cell.messageImageView.isHidden = true
            cell.textView.text = text
            cell.bubbleWidthAnchor?.constant = estimateFrameForText(text: text).width + 32
        }
        
        cell.playButton.isHidden = message.videoUrl == nil
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var height: CGFloat = 80
        
        let message = messages[indexPath.item]

        if let text = message.text {
            height = estimateFrameForText(text: text).height + 20
        } else if let imageWidth = message.imageWidth?.floatValue, let imageHeight = message.imageHeight?.floatValue {
            height = CGFloat(imageHeight / imageWidth * 200)
        }
    
        // can also use this width
        // let width = UIScreen.main.bounds.width
        return CGSize(width: view.frame.width, height: height)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    private func estimateFrameForText(text: String) -> CGRect {
        let size = CGSize(width: 200, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(NSStringDrawingOptions.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size, options: options, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 16)], context: nil)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    func setupInputComponents() {
        let containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.backgroundColor = UIColor.white
        
        view.addSubview(containerView)
        
        let uploadImageView = UIImageView()
        uploadImageView.isUserInteractionEnabled = true
        uploadImageView.image = UIImage(named: "upload_image_icon")
        uploadImageView.translatesAutoresizingMaskIntoConstraints = false
        uploadImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleUploadTap)))
        containerView.addSubview(uploadImageView)
        //x,y,w,h
        uploadImageView.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
        uploadImageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        uploadImageView.widthAnchor.constraint(equalToConstant: 44).isActive = true
        uploadImageView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        //ios9 constraint anchors
        //x,y,w,h
        containerView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        containerViewBottomAnchor = containerView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        containerViewBottomAnchor?.isActive = true
        containerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        containerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        let sendButton = UIButton(type: .system)
        sendButton.setTitle("Send", for: .normal)
        sendButton.translatesAutoresizingMaskIntoConstraints = false
        sendButton.addTarget(self, action: #selector(handleSend), for: .touchUpInside)
        containerView.addSubview(sendButton)
        //x,y,w,h
        sendButton.rightAnchor.constraint(equalTo: containerView.rightAnchor).isActive = true
        sendButton.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        sendButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
        sendButton.heightAnchor.constraint(equalTo: containerView.heightAnchor).isActive = true
        
        containerView.addSubview(inputTextField)
        //x,y,w,h
        inputTextField.leftAnchor.constraint(equalTo: uploadImageView.rightAnchor, constant: 8).isActive = true
        inputTextField.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        inputTextField.rightAnchor.constraint(equalTo: sendButton.leftAnchor).isActive = true
        inputTextField.heightAnchor.constraint(equalTo: containerView.heightAnchor).isActive = true
        
        let separatorLineView = UIView()
        separatorLineView.backgroundColor = UIColor(r: 220, g: 220, b: 220)
        separatorLineView.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(separatorLineView)
        //x,y,w,h
        separatorLineView.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
        separatorLineView.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        separatorLineView.widthAnchor.constraint(equalTo: containerView.widthAnchor).isActive = true
        separatorLineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
    
    func handleUploadTap() {
        let imagePickerController = UIImagePickerController()
        
        imagePickerController.allowsEditing = true
        imagePickerController.delegate = self
        imagePickerController.mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String]
        
        present(imagePickerController, animated: true, completion: nil)
    }
    
    // image picker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let videoUrl = info["UIImagePickerControllerMediaURL"] as? URL{
            uploadToFirebaseStorageUsingUrl(videoUrl: videoUrl)
            
        } else {
            var selectedImageFromPicker: UIImage?
            if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
                selectedImageFromPicker = editedImage
            } else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
                selectedImageFromPicker = originalImage
            }
            if let selectedImage = selectedImageFromPicker {
                uploadToFirebaseStorageUsingImage(image: selectedImage)
            }
        }

        dismiss(animated: true, completion: nil)
    }
    
    func uploadToFirebaseStorageUsingUrl(videoUrl: URL){
        activityIndicatorView.startAnimating()
        let videoName = NSUUID().uuidString
        let storageRef = FIRStorage.storage().reference().child("message_videos").child("\(videoName).mov")
        
        let uploadTask = storageRef.putFile(videoUrl, metadata: nil) { (metadata, error) in
            if error != nil {
                return
            }
            
            if let downloadUrl = metadata?.downloadURL()?.absoluteString {
                let imageName = NSUUID().uuidString
                let storageRef = FIRStorage.storage().reference().child("video_thumbnails").child("\(imageName).jpg")
                
                if let thumbnailImage = self.thumbnailImageForFileUrl(fileUrl: videoUrl){
                    if let uploadData = UIImageJPEGRepresentation(UIImage(cgImage: thumbnailImage), 0.2) {
                        storageRef.put(uploadData, metadata: nil, completion: { (metadata, error) in
                            if error != nil {
                                return
                            }
                            
                            if let imageUrl = metadata?.downloadURL()?.absoluteString {
                                self.sendMessageWithVideoUrl(videoUrl: downloadUrl, imageUrl: imageUrl, image: UIImage(cgImage: thumbnailImage))
                            }
                        })
                    }
                }
            }
        }
        uploadTask.observe(FIRStorageTaskStatus.progress) { (snapshot) in
            //progress
        }
        uploadTask.observe(FIRStorageTaskStatus.success) { (snapshot) in
            //success
        }
        
    }
    
    func sendMessageWithVideoUrl(videoUrl: String, imageUrl: String, image: UIImage) {
        let ref = FIRDatabase.database().reference().child("messages")
        let childRef = ref.childByAutoId()
        
        if let toId = user?.id, let fromId = FIRAuth.auth()?.currentUser?.uid {
            let timestamp = Int(Date().timeIntervalSince1970)
            let values = ["videoUrl": videoUrl, "imageUrl": imageUrl, "toId": toId, "fromId": fromId, "timestamp": timestamp, "imageWidth": image.size.width, "imageHeight": image.size.height] as [String : Any]
            
            childRef.updateChildValues(values) { (error, ref) in
                if error != nil {
                    return
                }
                
                let messageId = childRef.key
                
                let userMessagesRef = FIRDatabase.database().reference().child("user-messages").child(fromId).child(toId)
                userMessagesRef.updateChildValues([messageId: 1])
                let recipientUserMessagesRef = FIRDatabase.database().reference().child("user-messages").child(toId).child(fromId)
                recipientUserMessagesRef.updateChildValues([messageId: 1])
                DispatchQueue.main.async {
                    self.activityIndicatorView.stopAnimating()
                }
            }
        }
    }
    
    private func thumbnailImageForFileUrl(fileUrl: URL) -> CGImage? {
        let asset = AVAsset(url: fileUrl)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        let time = CMTime(value: 1, timescale: 60)
        let thumbnailCGImage = try? imageGenerator.copyCGImage(at: time, actualTime: nil)
        return thumbnailCGImage
    }
    
    func uploadToFirebaseStorageUsingImage(image: UIImage){
        activityIndicatorView.startAnimating()
        let imageName = NSUUID().uuidString
        let storageRef = FIRStorage.storage().reference().child("message_images").child("\(imageName).jpg")
        
        // can also use "uploadData = UIImagePNGRepresentation(self.profileImageView.image!)" but the quality will be high
        
        if let uploadData = UIImageJPEGRepresentation(image , 0.2){
            storageRef.put(uploadData, metadata: nil, completion: { (metadata, error) in
                if error != nil {
                    return
                }
                
                if let imageUrl = metadata?.downloadURL()?.absoluteString {
                    self.sendMessageWithImageUrl(imageUrl: imageUrl, image: image)
                }
            })
        }
    }
    
    private func sendMessageWithImageUrl(imageUrl: String, image: UIImage) {
        let ref = FIRDatabase.database().reference().child("messages")
        let childRef = ref.childByAutoId()
        
        if let toId = user?.id, let fromId = FIRAuth.auth()?.currentUser?.uid {
            let timestamp = Int(Date().timeIntervalSince1970)
            let values = ["imageUrl": imageUrl, "toId": toId, "fromId": fromId, "timestamp": timestamp, "imageWidth": image.size.width, "imageHeight": image.size.height] as [String : Any]
            
            childRef.updateChildValues(values) { (error, ref) in
                if error != nil {
                    return
                }
                
                let messageId = childRef.key
                
                let userMessagesRef = FIRDatabase.database().reference().child("user-messages").child(fromId).child(toId)
                userMessagesRef.updateChildValues([messageId: 1])
                let recipientUserMessagesRef = FIRDatabase.database().reference().child("user-messages").child(toId).child(fromId)
                recipientUserMessagesRef.updateChildValues([messageId: 1])
                self.activityIndicatorView.stopAnimating()
            }
        }

    }
    
    func uploadToFirebaseStorageUsingImage(image: UIImage, timeLimit: Int){
        let imageName = NSUUID().uuidString
        let storageRef = FIRStorage.storage().reference().child("message_images").child("\(imageName).jpg")
        
        // can also use "uploadData = UIImagePNGRepresentation(self.profileImageView.image!)" but the quality will be high
        
        if let uploadData = UIImageJPEGRepresentation(image , 0.2){
            storageRef.put(uploadData, metadata: nil, completion: { (metadata, error) in
                if error != nil {
                    return
                }
                
                if let imageUrl = metadata?.downloadURL()?.absoluteString {
                    self.sendMessageWithImageUrl(imageUrl: imageUrl, image: image, timeLimit: timeLimit)
                }
            })
        }
    }
    
    private func sendMessageWithImageUrl(imageUrl: String, image: UIImage, timeLimit: Int) {
        let ref = FIRDatabase.database().reference().child("messages")
        let childRef = ref.childByAutoId()
        
        if let toId = user?.id, let fromId = FIRAuth.auth()?.currentUser?.uid {
            let timestamp = Int(Date().timeIntervalSince1970)
            let values = ["imageUrl": imageUrl, "toId": toId, "fromId": fromId, "timestamp": timestamp, "imageWidth": image.size.width, "imageHeight": image.size.height, "timeLimit": timeLimit, "readCount": 2] as [String : Any]
            
            childRef.updateChildValues(values) { (error, ref) in
                if error != nil {
                    return
                }
                
                let messageId = childRef.key
                
                let userMessagesRef = FIRDatabase.database().reference().child("user-messages").child(fromId).child(toId)
                userMessagesRef.updateChildValues([messageId: 1])
                let recipientUserMessagesRef = FIRDatabase.database().reference().child("user-messages").child(toId).child(fromId)
                recipientUserMessagesRef.updateChildValues([messageId: 1])
            }
        }
        
    }

    
    func handleSend() {
        let ref = FIRDatabase.database().reference().child("messages")
        let childRef = ref.childByAutoId()
        
        if let toId = user?.id, let fromId = FIRAuth.auth()?.currentUser?.uid, let text = inputTextField.text , text != "" {
            let timestamp = Int(Date().timeIntervalSince1970)
            let values = ["text": text, "toId": toId, "fromId": fromId, "timestamp": timestamp] as [String : Any]

            childRef.updateChildValues(values) { (error, ref) in
                if error != nil {
                    return
                }
                self.inputTextField.text = nil
                
                let messageId = childRef.key

                let userMessagesRef = FIRDatabase.database().reference().child("user-messages").child(fromId).child(toId)
                userMessagesRef.updateChildValues([messageId: 1])
                let recipientUserMessagesRef = FIRDatabase.database().reference().child("user-messages").child(toId).child(fromId)
                recipientUserMessagesRef.updateChildValues([messageId: 1])
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        handleSend()
        textField.resignFirstResponder()
        return true
    }
    
    func performZoomInForStartingImageView(startingImageView: UIImageView, message: Message) {
        let storyboard = UIStoryboard(name: "Memories", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ImageDisplayController") as! ImageDisplayController
        vc.imageToDisplay = startingImageView.image
        vc.message = message
        present(vc, animated: false, completion: nil)
        
    }
}













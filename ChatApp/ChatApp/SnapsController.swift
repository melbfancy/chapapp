//
//  SnapsController.swift
//  ChatApp
//
//  Created by Fan Qi on 16/10/16.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import UIKit
import Firebase

class SnapsController: UIViewController, UserSpecificSetupProtocol {
    
    var memories = [Memory]()
    
    var imageViews = [MemoryImageView]()
    
    lazy var sendButton: UIButton = {
        let button = UIButton()
        button.setTitle("Create Story", for: UIControlState.normal)
        button.backgroundColor = UIColor(r: 21, g: 139, b: 201)
        button.setTitleColor(UIColor.white, for: UIControlState.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 22)
        button.addTarget(self, action: #selector(handleCreateStory), for: UIControlEvents.touchUpInside)
        button.isHidden = true
        return button
    }()
    
    var imageViewsToSend = [MemoryImageView]()
    
    var timer: Timer?

    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupGeneralAspects()
        // setupUserSpecificAspects()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupGeneralAspects() {
        scrollView.frame = view.bounds
        scrollView.backgroundColor = UIColor.clear
        scrollView.contentSize = view.bounds.size
        scrollView.contentOffset = CGPoint(x: 0, y: 0)
        sendButton.frame = CGRect(x: 50, y: view.bounds.height - 130, width: view.bounds.width - 100, height: 45)
        view.addSubview(sendButton)
    }
    
    func setupUserSpecificAspects() {
        for imageView in imageViews {
            imageView.removeFromSuperview()
        }
        memories = [Memory]()
        imageViews = [MemoryImageView]()
        fetchSnaps()
    }
    
    func fetchSnaps() {
        guard let uid = FIRAuth.auth()?.currentUser?.uid else {
            return
        }
        
        let userMemoriesRef = FIRDatabase.database().reference().child("user-memories").child(uid)
        userMemoriesRef.observe(FIRDataEventType.childAdded, with: { (snapshot) in
            let memoryId = snapshot.key
            let memoriesRef = FIRDatabase.database().reference().child("memories").child(memoryId)
            memoriesRef.observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                guard let dictionary = snapshot.value as? [String: AnyObject] else {
                    return
                }
                
                let memory = Memory()
                memory.id = memoryId
                memory.setValuesForKeys(dictionary)
                
                self.memories.append(memory)
                let imageView = MemoryImageView()
                let imageWidth = self.view.bounds.width / 3
                let imageHeight = self.view.bounds.height / 3
                let index = self.memories.count - 1
                imageView.memory = memory
                imageView.isUserInteractionEnabled = true
                imageView.contentMode = .scaleAspectFit
                imageView.layer.borderWidth = 2
                imageView.layer.borderColor = UIColor.white.cgColor
                imageView.layer.cornerRadius = 10
                imageView.layer.masksToBounds = true
                imageView.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(self.handlePress(recognizer: ))))
                imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleTap(recognizer: ))))
                self.imageViews.append(imageView)
                
                DispatchQueue.main.async {
                    imageView.frame = CGRect(x: CGFloat(index % 3) * imageWidth, y: CGFloat(index / 3) * imageHeight , width: imageWidth, height: imageHeight)
                    self.scrollView.contentSize = CGSize(width: self.view.bounds.width, height: CGFloat(index / 3 + 1) * imageHeight)
                    self.scrollView.addSubview(imageView)
                }
            })
        })
    }
    
    func handlePress(recognizer: UILongPressGestureRecognizer) {
        if let targetView = recognizer.view as? MemoryImageView {
            let memoryDisplayController = MemoryDisplayController()
            memoryDisplayController.image = targetView.image
            memoryDisplayController.snapsController = self
            memoryDisplayController.memory = targetView.memory
            present(memoryDisplayController, animated: false, completion: nil)
        }
    }
    
    func handleTap(recognizer: UITapGestureRecognizer) {
        if let targetView = recognizer.view as? MemoryImageView {
            if targetView.isSelected {
                targetView.tickView.removeFromSuperview()
                targetView.isSelected = false
                filterImageViews()
                if imageViewsToSend.count == 0 {
                    sendButton.isHidden = true
                }
            } else {
                let width = targetView.bounds.width
                let height = targetView.bounds.height
                targetView.tickView.frame = CGRect(x: width / 2 - 20, y: height / 2 - 20, width: 40, height: 40)
                targetView.addSubview(targetView.tickView)
                targetView.isSelected = true
                filterImageViews()
                sendButton.isHidden = false
            }
        }

    }
    
    func filterImageViews() {
        imageViewsToSend = imageViews.filter({ (imageView) -> Bool in
            return imageView.isSelected
        })
    }
    
    func handleCreateStory() {
        let ref = FIRDatabase.database().reference().child("memory-stories")
        let childRef = ref.childByAutoId()
        
        if let uid = FIRAuth.auth()?.currentUser?.uid {
            var values: [String : Any] = [String : Any]()
            for imageView in imageViewsToSend {
                if let id = imageView.memory?.id, let url = imageView.memory?.imageUrl {
                    values[id] = url
                }
            }
            
            childRef.updateChildValues(values) { (error, ref) in
                if error != nil {
                    return
                }
                
                let memoryStoryId = childRef.key
                
                let userMessagesRef = FIRDatabase.database().reference().child("user-memory-stories").child(uid)
                userMessagesRef.updateChildValues([memoryStoryId: 1])
            }
        }

    }
    
    func handleRemove(memory: Memory) {
        guard let uid = FIRAuth.auth()?.currentUser?.uid else {
            return
        }
        let userMemorysRef = FIRDatabase.database().reference().child("user-memories").child(uid)
        let memoryRef = userMemorysRef.child(memory.id!)
        memoryRef.removeValue()
        userMemorysRef.removeAllObservers()
        
        DispatchQueue.main.async {
            self.setupUserSpecificAspects()
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

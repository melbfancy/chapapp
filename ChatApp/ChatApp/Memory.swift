//
//  Memory.swift
//  ChatApp
//
//  Created by Fan Qi on 16/10/16.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import Foundation

class Memory: NSObject {
    
    var id: String?
    var uid: String?
    var timestamp: NSNumber?
    var imageUrl: String?
    var imageHeight: NSNumber?
    var imageWidth: NSNumber?
    
}

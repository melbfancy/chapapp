//
//  CameraRollController.swift
//  ChatApp
//
//  Created by Fan Qi on 16/10/16.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import UIKit
import Photos

class CameraRollController: UIViewController, UserSpecificSetupProtocol {
    
    var assets: PHFetchResult<PHAsset>?
    
    var imageViews = [MemoryImageView]()
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupGeneralAspects()
        setupUserSpecificAspects()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupGeneralAspects() {
        scrollView.frame = view.bounds
        scrollView.backgroundColor = UIColor.clear
        scrollView.contentSize = view.bounds.size
        scrollView.contentOffset = CGPoint(x: 0, y: 0)
        if PHPhotoLibrary.authorizationStatus() == .authorized {
            reloadAssets()
        } else {
            PHPhotoLibrary.requestAuthorization({ (status: PHAuthorizationStatus) -> Void in
                if status == .authorized {
                    self.reloadAssets()
                } else {
                    self.showNeedAccessMessage()
                }
            })
        }
    }
    
    func setupUserSpecificAspects() {
    }
    
    private func showNeedAccessMessage() {
        let alert = UIAlertController(title: "Image picker", message: "App need get access to photos", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil))
        present(alert, animated: false, completion: nil)
    }

    private func reloadAssets() {
        for imageView in imageViews {
            imageView.removeFromSuperview()
        }
        imageViews = [MemoryImageView]()
        assets = nil
        
        let imageWidth = self.view.bounds.width / 3
        let imageHeight = self.view.bounds.height / 3
        
        assets = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: nil)
        assets?.enumerateObjects({ (asset, index, pointer) in
            PHImageManager.default().requestImage(for: asset, targetSize: CGSize(width: imageWidth, height: imageHeight), contentMode: PHImageContentMode.aspectFit, options: nil, resultHandler: { (image, dict) in
                let imageView = MemoryImageView()

                imageView.isUserInteractionEnabled = true
                imageView.contentMode = .scaleAspectFit
                imageView.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(self.handlePress(recognizer: ))))
                imageView.image = image
                self.imageViews.append(imageView)
                
                DispatchQueue.main.async {
                    imageView.frame = CGRect(x: CGFloat(index % 3) * imageWidth, y: CGFloat(index / 3) * imageHeight , width: imageWidth, height: imageHeight)
                    self.scrollView.contentSize = CGSize(width: self.view.bounds.width, height: CGFloat(index / 3 + 1) * imageHeight)
                    self.scrollView.addSubview(imageView)
                }

            })
        })
    }
    
    func handlePress(recognizer: UILongPressGestureRecognizer) {
        if let targetView = recognizer.view as? MemoryImageView {
            let memoryDisplayController = MemoryDisplayController()
            memoryDisplayController.image = targetView.image
            memoryDisplayController.cameraRollController = self
            present(memoryDisplayController, animated: false, completion: nil)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  CameraController.swift
//  camera
//
//  Created by shengshuo zhang on 2016/10/2. Modified by Qi Fan.
//  Copyright © 2016年 shengshuo zhang. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices
import MediaPlayer

class CameraController: UIImagePickerController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var redirectDelegate: ContainerController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
            delegate = self
            sourceType = UIImagePickerControllerSourceType.camera
            allowsEditing = false
            mediaTypes = [kUTTypeMovie as String, kUTTypeImage as String]
        } else {
            delegate = self
            allowsEditing = false
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            redirectDelegate?.image = image
            redirectDelegate?.switchToEditingController()
        }
        if let video = info[UIImagePickerControllerMediaURL] as? NSURL{
            redirectDelegate?.video = video
            redirectDelegate?.switchToEditingController()
        }
    }
    
}

//
//  BarcodeScannerController.swift
//  ChatApp
//
//  Created by Fan Qi on 16/10/10.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import UIKit
import AVFoundation

class BarcodeScannerController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    var objCaptureSession:AVCaptureSession?
    var objCaptureVideoPreviewLayer:AVCaptureVideoPreviewLayer?
    var vwQRCode:UIView?
    var text: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureVideoCapture()
    }
    
    func configureVideoCapture() {
        let objCaptureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        var error:NSError?
        let objCaptureDeviceInput: AnyObject?
        
        do {
            objCaptureDeviceInput = try AVCaptureDeviceInput(device: objCaptureDevice) as AVCaptureDeviceInput
            
        } catch let error1 as NSError {
            error = error1
            objCaptureDeviceInput = nil
        }
        if (error != nil) {
            let alertController =  UIAlertController(title: "Device Error", message: "Device not Supported for this Application", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: { (action) in
                _ = self.navigationController?.popViewController(animated: true)
            }))
            present(alertController, animated: true, completion: nil)
            return
        }
        objCaptureSession = AVCaptureSession()
        
        if let deviceInput = objCaptureDeviceInput as? AVCaptureInput {
            objCaptureSession?.addInput(deviceInput)
            let objCaptureMetadataOutput = AVCaptureMetadataOutput()
            objCaptureSession?.addOutput(objCaptureMetadataOutput)
            objCaptureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            objCaptureMetadataOutput.metadataObjectTypes = [AVMetadataObjectTypeQRCode]
            addVideoPreviewLayer()
            initializeQRView()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        vwQRCode?.frame = CGRect.zero
        objCaptureSession?.startRunning()
    }
    
    func addVideoPreviewLayer()
    {
        objCaptureVideoPreviewLayer = AVCaptureVideoPreviewLayer(session: objCaptureSession)
        if let captureVideoPreviewLayer = objCaptureVideoPreviewLayer {
            captureVideoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
            captureVideoPreviewLayer.frame = view.layer.bounds
            self.view.layer.addSublayer(captureVideoPreviewLayer)
//            objCaptureSession?.startRunning()
        }
    }
    
    func initializeQRView() {
        vwQRCode = UIView()
        
        if let qrCode = vwQRCode {
            qrCode.layer.borderColor = UIColor.green.cgColor
            qrCode.layer.borderWidth = 5
            self.view.addSubview(qrCode)
            self.view.bringSubview(toFront: qrCode)
        }
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        if metadataObjects == nil || metadataObjects.count == 0 {
            vwQRCode?.frame = CGRect.zero
            text = ""
            return
        }
        if let objMetadataMachineReadableCodeObject = metadataObjects[0] as? AVMetadataMachineReadableCodeObject {
            if objMetadataMachineReadableCodeObject.type == AVMetadataObjectTypeQRCode {
                if let objBarcode = objCaptureVideoPreviewLayer?.transformedMetadataObject(for: objMetadataMachineReadableCodeObject) as? AVMetadataMachineReadableCodeObject {
                    vwQRCode?.frame = objBarcode.bounds
                }
                if objMetadataMachineReadableCodeObject.stringValue != nil {
                    objCaptureSession?.stopRunning()
                    text = objMetadataMachineReadableCodeObject.stringValue
                    let singleResultController = SingleResultController()
                    singleResultController.userId = text
                    singleResultController.navigationItem.title = text
                    Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (timer) in
                        self.navigationController?.pushViewController(singleResultController, animated: true)
                    })
                }
            }
        }
    }
}

//
//  RequestsController.swift
//  ChatApp
//
//  Created by Fan Qi on 16/10/8.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import UIKit
import Firebase

class RequestsController: UITableViewController, FriendCellButtonProtocol {
    
    var users = [User]()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(FriendCell.self, forCellReuseIdentifier: "reuseIdentifier")
        fetchRequests()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    func fetchRequests() {
        if let uid = FIRAuth.auth()?.currentUser?.uid {
            let userRequestsRef = FIRDatabase.database().reference().child("user-requests").child(uid)
            userRequestsRef.observe(FIRDataEventType.childAdded, with: { (snapshot) in
                let requesterId = snapshot.key
                let requesterRef = FIRDatabase.database().reference().child("users").child(requesterId)
                requesterRef.observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                    guard let dictionary = snapshot.value as? [String: AnyObject] else {
                        return
                    }
                    let user = User()
                    user.id = snapshot.key
                    // a convinient way to set all the properties
                    // if unmatched, will crash
                    user.setValuesForKeys(dictionary)
                    self.users.append(user)
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                })
            })
            userRequestsRef.observe(FIRDataEventType.childRemoved, with: { (snapshot) in
                self.users = self.users.filter({ (user) -> Bool in
                    return user.id != snapshot.key
                })
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            })
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return users.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! FriendCell
        
        // Configure the cell...
        let user = users[indexPath.row]
        cell.user = user
        cell.buttonDelegate = self
        cell.setupButton(title: "Accept")
        cell.setupCell()
        return cell
    }
    
    func performOperation(user: User) {
        guard let uid = FIRAuth.auth()?.currentUser?.uid, let requesterId = user.id else {
            return
        }
        let ref = FIRDatabase.database().reference().child("relations").child(uid)
        let requesterRef = FIRDatabase.database().reference().child("relations").child(requesterId)
        ref.updateChildValues([requesterId : true])
        requesterRef.updateChildValues([uid : true])
        FIRDatabase.database().reference().child("user-requests").child(uid).child(requesterId).removeValue()
    }

}

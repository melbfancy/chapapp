//
//  UserSpecificSetupProtocol.swift
//  ChatApp
//
//  Created by Fan Qi on 16/10/17.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import Foundation

protocol UserSpecificSetupProtocol {
    func setupUserSpecificAspects()
}

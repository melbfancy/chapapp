//
//  MemoryDisplayController.swift
//  ChatApp
//
//  Created by Fan Qi on 16/10/16.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import UIKit
import Firebase

class MemoryDisplayController: UIViewController {

    var memory: Memory?
    
    var snapsController: SnapsController?
    
    var cameraRollController: CameraRollController?
    
    var image: UIImage?

    
    let activityIndicatorView: UIActivityIndicatorView = {
        let aiv = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        aiv.hidesWhenStopped = true
        return aiv
    }()
    
    lazy var backGroundImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    lazy var cancelButton: UIButton = {
        let button = UIButton(type: UIButtonType.system)
        button.setTitle("X", for: UIControlState.normal)
        button.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor(r: 21, g: 139, b: 201), for: UIControlState.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        button.addTarget(self, action: #selector(handleReturn), for: UIControlEvents.touchUpInside)
        return button
    }()

    
    lazy var storyButton: UIButton = {
        let button = UIButton()
        button.setTitle("S", for: UIControlState.normal)
        button.addTarget(self, action: #selector(handleSaveStory), for: UIControlEvents.touchUpInside)
        button.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor(r: 21, g: 139, b: 201), for: UIControlState.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        return button
    }()
    
    
    lazy var shareButton: UIButton = {
        let button = UIButton()
        button.setTitle("Share", for: UIControlState.normal)
        button.addTarget(self, action: #selector(handleShare), for: UIControlEvents.touchUpInside)
        button.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor(r: 21, g: 139, b: 201), for: UIControlState.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        return button
    }()
    
    lazy var removeButton: UIButton = {
        let button = UIButton()
        button.setTitle("D", for: UIControlState.normal)
        button.addTarget(self, action: #selector(handleRemove), for: UIControlEvents.touchUpInside)
        button.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor(r: 21, g: 139, b: 201), for: UIControlState.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        return button
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupGeneralAspects()
    }
    
    func setupGeneralAspects() {
        
        view.backgroundColor = UIColor.black
        
        backGroundImageView.frame = view.bounds
        backGroundImageView.image = image
        backGroundImageView.backgroundColor = UIColor.black
        
        
        cancelButton.frame = CGRect(x: 20, y: 10, width: 50, height: 50)
        removeButton.frame = CGRect(x: 20, y: view.bounds.height - 60, width: 50, height: 50)
        shareButton.frame = CGRect(x: view.bounds.width - 100, y: view.bounds.height - 60, width: 80, height: 50)
        storyButton.frame = CGRect(x: 80, y: view.bounds.height - 60, width: 50, height: 50)
        activityIndicatorView.frame = CGRect(x: view.bounds.width/2 - 20, y: view.bounds.height/2 - 20, width: 40, height: 40)
        
        view.addSubview(backGroundImageView)
        view.addSubview(cancelButton)
        view.addSubview(shareButton)
        view.addSubview(removeButton)
        view.addSubview(storyButton)
        view.addSubview(activityIndicatorView)
        
    }
    
    func handleRemove() {
        dismiss(animated: false) {
            if self.snapsController != nil {
                self.snapsController?.handleRemove(memory: self.memory!)
            }
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func handleReturn() {
        dismiss(animated: false, completion: nil)
    }
    
    func handleShare() {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            let friendsController = FriendsController()
            friendsController.messagesController = appDelegate.leftController
            friendsController.imageToSend = image
            friendsController.timeLimit = 5
            let navigationController = UINavigationController(rootViewController: friendsController)
            present(navigationController, animated: true, completion: nil)
            
        }
    }
    
    func handleSaveStory() {
        uploadToFirebaseStorageUsingImageForStory(image: image!)
    }
    
    
    func uploadToFirebaseStorageUsingImageForStory(image: UIImage){
        activityIndicatorView.startAnimating()
        let imageName = NSUUID().uuidString
        let storageRef = FIRStorage.storage().reference().child("story_images").child("\(imageName).jpg")
        
        // can also use "uploadData = UIImagePNGRepresentation(self.profileImageView.image!)" but the quality will be high
        
        if let uploadData = UIImageJPEGRepresentation(image , 0.2){
            storageRef.put(uploadData, metadata: nil, completion: { (metadata, error) in
                if error != nil {
                    return
                }
                
                if let imageUrl = metadata?.downloadURL()?.absoluteString {
                    self.sendStoryWithImageUrl(imageUrl: imageUrl, image: image)
                }
            })
        }
    }
    
    private func sendStoryWithImageUrl(imageUrl: String, image: UIImage) {
        let ref = FIRDatabase.database().reference().child("stories")
        let childRef = ref.childByAutoId()
        
        if let uid = FIRAuth.auth()?.currentUser?.uid {
            let timestamp = Int(Date().timeIntervalSince1970)
            let values = ["imageUrl": imageUrl, "uid": uid, "timestamp": timestamp, "imageWidth": image.size.width, "imageHeight": image.size.height] as [String : Any]
            
            childRef.updateChildValues(values) { (error, ref) in
                if error != nil {
                    return
                }
                
                let storyId = childRef.key
                
                let userStoriesRef = FIRDatabase.database().reference().child("user-stories").child(uid)
                userStoriesRef.updateChildValues([storyId: 1])
                self.activityIndicatorView.stopAnimating()
            }
        }
        
    }


}

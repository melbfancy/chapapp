//
//  User.swift
//  ChatApp
//
//  Created by Fan Qi on 16/9/27.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import Foundation

class User: NSObject {
    
    var id: String?
    var name: String?
    var email: String?
    var profileImageUrl: String?
    var phoneNumber: String?
    var isRegistered: Bool?
    var barcodeImageUrl: String?

}

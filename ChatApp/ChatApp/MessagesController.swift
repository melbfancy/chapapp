//
//  ViewController.swift
//  ChatApp
//
//  Created by Fan Qi on 16/9/26.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import UIKit
import Firebase

class MessagesController: UITableViewController {
    
    let cellId = "cellId"
    
    var messages = [Message]()
    
    var messagesDictionary = [String: Message]()
    
    var timer: Timer?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupGeneralAspects()
        // setupUserSpecificAspects()
    }
    
    func setupUserSpecificAspects(){
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            if let user = appDelegate.currentUser {
                navigationItem.title = user.name
                messages.removeAll()
                messagesDictionary.removeAll()
                tableView.reloadData()
                observeUserMessages()
            }
        }
    }
    
    func setupGeneralAspects() {
        let image = UIImage(named: "new_message_icon")
        let cameraIcon = UIImage(named: "camera_icon")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.plain, target: self, action: #selector(handleNewMessage))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: cameraIcon, style: UIBarButtonItemStyle.plain, target: self, action: #selector(handleScrollToCamera))
        tableView.register(UserCell.self, forCellReuseIdentifier: cellId)
    }
    
    func handleScrollToCamera() {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.snapchatCrossPagerViewController?.scrollToPage(page: 1)
        }
    }
    
    func observeUserMessages() {
        guard let uid = FIRAuth.auth()?.currentUser?.uid else {
            return
        }
        let ref = FIRDatabase.database().reference().child("user-messages").child(uid)
        ref.observe(FIRDataEventType.childAdded, with: { (snapshot) in
            let userId = snapshot.key
            FIRDatabase.database().reference().child("user-messages").child(uid).child(userId).observe(FIRDataEventType.childAdded, with: { (snapshot) in
                let messageId = snapshot.key
                self.fetchMessageWithMessageId(messageId: messageId)
            })
        })
    }
    
    private func fetchMessageWithMessageId(messageId: String) {
        let messagesReference  = FIRDatabase.database().reference().child("messages").child(messageId)
        messagesReference.observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let message = Message(dictionary: dictionary)
                // potential of crashing if keys don't match
                // another convinient way of setting properties
                // message.setValuesForKeys(dictionary)
                if let chatParterId = message.chatPartnerId() {
                    self.messagesDictionary[chatParterId] = message
                }
                self.attemptReloadOfTable()
            }
        })
    }
    
    private func attemptReloadOfTable() {
        self.timer?.invalidate()
        
        self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.handleReloadTable), userInfo: nil, repeats: false)
    }
    
    func handleReloadTable() {
        self.messages = Array(self.messagesDictionary.values)
        
        self.messages.sort(by: { (message1, message2) -> Bool in
            return (message1.timestamp?.intValue)! > (message2.timestamp?.intValue)!
        })
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! UserCell
        
        let message = messages[indexPath.row]
        cell.message = message
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let message = messages[indexPath.row]
        
        guard let chatPartnerId = message.chatPartnerId() else {
            return
        }
        
        let ref = FIRDatabase.database().reference().child("users").child(chatPartnerId)
        ref.observeSingleEvent(of: FIRDataEventType.value, with: {  (snapshot) in
            guard let dictionary = snapshot.value as? [String: AnyObject] else {
                return
            }
            
            let user = User()
            user.id = chatPartnerId
            user.setValuesForKeys(dictionary)
            self.showChatController(for: user)
        
        })
    }
    
    func handleNewMessage(){
        let friendsController = FriendsController()
        friendsController.messagesController = self
        let navigationController = UINavigationController(rootViewController: friendsController)
        present(navigationController, animated: true, completion: nil)
    }
    
    func showChatController(for user: User) {
        let chatLogController = ChatLogController(collectionViewLayout: UICollectionViewFlowLayout())
        chatLogController.user = user
        let navigationController = UINavigationController(rootViewController: chatLogController)
        present(navigationController, animated: true, completion: nil)
    }
    
    func handleNewMessageWithImage(image: UIImage, timeLimit: Int){
        let friendsController = FriendsController()
        friendsController.messagesController = self
        friendsController.imageToSend = image
        friendsController.timeLimit = timeLimit
        let navigationController = UINavigationController(rootViewController: friendsController)
        present(navigationController, animated: true, completion: nil)
    }
    
}


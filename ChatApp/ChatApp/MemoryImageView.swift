//
//  MemoryImageView.swift
//  ChatApp
//
//  Created by Fan Qi on 16/10/16.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import UIKit

class MemoryImageView: UIImageView {
    
    var isSelected: Bool = false
    
    var tickView: UIImageView = {
        let image = UIImage(named: "tick_icon")
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        view.image = image
        return view
    }()

    var memory: Memory? {
        didSet {
            if let imageUrl = memory?.imageUrl {
                loadImageUsingCacheWithUrlString(urlString: imageUrl)
            }
        }
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

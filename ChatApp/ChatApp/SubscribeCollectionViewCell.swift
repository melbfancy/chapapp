//
//  SubscribeCollectionViewCell.swift
//  ChatStories
//
//  Created by Liu Sam on 10/10/16.
//  Copyright © 2016 Liu Sam. All rights reserved.
//

import UIKit

class SubscribeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var userLabel: UILabel!
}

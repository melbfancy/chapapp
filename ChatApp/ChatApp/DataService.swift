//
//  DataService.swift
//  ChatStories
//
//  Created by Liu Sam on 10/2/16.
//  Copyright © 2016 Liu Sam. All rights reserved.
//

import Foundation
import Firebase

let DB_BASE = FIRDatabase.database().reference()

class DataService {
    static let ds = DataService()
    
    private var _REF_STORY_PUBLIC = DB_BASE.child("story-public")
    private var _REF_STORY_PRIVATE = DB_BASE.child("stories")
    private var _REF_SUBSCRIBE = DB_BASE.child("subscribe")
    private var _REF_USER_PUBLIC = DB_BASE.child("public-user")
    private var _REF_USER = DB_BASE.child("users")
    private var _REF_USER_STORY = DB_BASE.child("user-stories")
    private var _REF_USER_RELATION = DB_BASE.child("relations")
    private var _REF_TOPIC = DB_BASE.child("topic")
    
    var REF_STORY_PUBLIC: FIRDatabaseReference {
        return _REF_STORY_PUBLIC
    }
    
    var REF_STORY_PRIVATE: FIRDatabaseReference {
        return _REF_STORY_PRIVATE
    }
    
    var REF_SUBSCRIBE: FIRDatabaseReference {
        return _REF_SUBSCRIBE
    }
    
    var REF_USER: FIRDatabaseReference {
        return _REF_USER
    }
    
    var REF_USER_PUBLIC: FIRDatabaseReference {
        return _REF_USER_PUBLIC
    }
    
    var REF_USER_STORY: FIRDatabaseReference {
        return _REF_USER_STORY
    }
    
    var REF_USER_RELATION: FIRDatabaseReference {
        return _REF_USER_RELATION
    }
    
    var REF_TOPIC: FIRDatabaseReference {
        return _REF_TOPIC
    }

    
    
    func increaseUserRating(userID: String, byValue: Int) {
        DataService.ds.REF_USER_PUBLIC.child(userID).child("rating").runTransactionBlock({
            (currentData:FIRMutableData!) in
            var value = currentData.value as? Int
            if value == nil {
                value = 0
            }
            currentData.value = value! + byValue
            return FIRTransactionResult.success(withValue: currentData)
        })
    }
    
}

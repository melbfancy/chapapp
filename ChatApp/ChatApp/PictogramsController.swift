//
//  PictogramsController.swift
//  TextEmojiDemo
//
//  Created by Fan Qi on 16/10/12.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import UIKit

class PictogramsController: UIViewController {
    
    var editingController: EditingController?
    
    let pictogram1: UIImageView = {
        let image = UIImage(named: "pictogram1")
        let view = UIImageView(image: image)
        view.contentMode = .scaleAspectFit
        view.isUserInteractionEnabled = true
        return view
    }()
    
    let pictogram2: UIImageView = {
        let image = UIImage(named: "pictogram2")
        let view = UIImageView(image: image)
        view.contentMode = .scaleAspectFit
        view.isUserInteractionEnabled = true
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupGeneralAspects()

        // Do any additional setup after loading the view.
    }
    
    func setupGeneralAspects(){
        view.backgroundColor = UIColor.white
        view.addSubview(pictogram1)
        view.addSubview(pictogram2)
        pictogram1.frame = CGRect(x: 20, y: 20, width: view.bounds.width/4, height: view.bounds.height/4)
        pictogram2.frame = CGRect(x: 20 + view.bounds.width/4 + 20, y: 20, width: view.bounds.width/4, height: view.bounds.height/4)
        
        pictogram1.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(addPictogram(recognizer:))))
        pictogram2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(addPictogram(recognizer:))))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func addPictogram(recognizer: UITapGestureRecognizer) {
        let view = recognizer.view
        view?.removeGestureRecognizer(recognizer)
        dismiss(animated: true) {
            self.editingController!.handlePictogram(view: view as! UIImageView)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  ContainerController.swift
//  camera
//
//  Created by shengshuo zhang on 2016/10/4. Modified by Qi Fan.
//  Copyright © 2016年 shengshuo zhang. All rights reserved.
//

import UIKit

class ContainerController: UIViewController {
    
    var cameraController: CameraController?
    var editingController: EditingController?
    var image: UIImage?
    var video: NSURL?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupCamera()
    }
    
    func setupCamera() {
        cameraController = CameraController()
        cameraController?.redirectDelegate = self
        addChildViewController(cameraController!)
        view.addSubview(cameraController!.view)
        cameraController?.didMove(toParentViewController: self)
    }
    
    func setupEditing() {
        editingController = EditingController()
        editingController?.redirectDelegate = self
        editingController?.image = image
        editingController?.video = video
        addChildViewController(editingController!)
        view.addSubview(editingController!.view)
        editingController?.didMove(toParentViewController: self)
    }
    
    func switchToEditingController(){
        cameraController?.willMove(toParentViewController: nil)
        cameraController?.view.removeFromSuperview()
        cameraController?.removeFromParentViewController()
        setupEditing()
    }
    
    func switchToCameraController(){
        editingController?.willMove(toParentViewController: nil)
        editingController?.view.removeFromSuperview()
        editingController?.removeFromParentViewController()
        setupCamera()
    }
    

}

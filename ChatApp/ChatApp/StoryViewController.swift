//
//  StoryViewController.swift
//  ChatStories
//
//  Created by Liu Sam on 9/30/16.
//  Copyright © 2016 Liu Sam. All rights reserved.
//

import UIKit
import Firebase

class StoryViewController: UIViewController {
    
    
    @IBOutlet weak var storyView: StoryView!
    
    @IBOutlet weak var storyTableView: UITableView!
    
    
    fileprivate let sectionInsets = UIEdgeInsets(top: 5.0, left: 5.0, bottom: 5.0, right: 5.0)
    fileprivate let itemsPerRow: CGFloat = 4
    var stories = [Stories]()
    var filteredStories = [Stories]()
    var filteredTopic = [Stories]()
    var userStories = [Stories]()
    var subscribeStories = [Stories]()
    var liveStory: Stories?
    var topic: [String:[String]] = [:]
    var topicString = ""
    
    var searchText: String? {
        didSet {
            filterStory()
            filterTopic()
            storyTableView.reloadData()
        }
    }
    
    enum Sections: Int {
        case discover
        case allStory
        case subscribe
        case live
    }
    
    let searchController = UISearchController(searchResultsController: nil)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // search bar
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.extendedLayoutIncludesOpaqueBars = true
        searchController.searchBar.searchBarStyle = UISearchBarStyle.minimal
        searchController.searchBar.sizeToFit()
        searchController.delegate = self
        definesPresentationContext = false
        storyTableView.tableHeaderView = searchController.searchBar
        
        // hide search bar
        storyTableView.setContentOffset(CGPoint(x: 0, y: storyTableView.tableHeaderView!.frame.size.height - storyTableView.contentInset.top), animated: false)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func showCamera(_ sender: AnyObject) {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.snapchatCrossPagerViewController?.scrollToPage(page: 1)
        }
    }
    
    
    @IBAction func showDiscover(_ sender: UIButton) {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.snapchatCrossPagerViewController?.scrollToPage(page: 3)
        }
    }
    
    func setupUserSpecificAspects() {
        observeStories()

    }
    
    func observeStories() {
        // subscribtion
        if let uid = FIRAuth.auth()?.currentUser?.uid {
            DataService.ds.REF_SUBSCRIBE.child(uid).observe(.value, with: { (snapshot) in
                if let snapshot = snapshot.children.allObjects as? [FIRDataSnapshot] {
                    self.subscribeStories.removeAll()
                    for snap in snapshot {
                        let userkey = snap.key
                        DataService.ds.REF_STORY_PUBLIC.child(userkey).observeSingleEvent(of: .value, with: { (snapshot) in
                            let userID = snapshot.key
                            if let snapshot = snapshot.children.allObjects as? [FIRDataSnapshot] {
                                var storyArr = [Story]()
                                for story in snapshot {
                                    if let storyDict = story.value as? Dictionary<String, AnyObject> {
                                        let key = story.key
                                        let story = Story(storyId: key, storyData: storyDict)
                                        storyArr.append(story)
                                    }
                                }
                                DataService.ds.REF_USER_PUBLIC.child(userID).observeSingleEvent(of: .value, with: { (snapshot) in
                                    if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                                        let user = User()
                                        user.id = snapshot.key
                                        user.name = userDict["name"] as? String
                                        let storiesItem = Stories(user: user, storiesData: storyArr)
                                        self.subscribeStories.append(storiesItem)
                                    }
                                })
                            }
                            self.storyTableView?.reloadData()
                        })
                    }
                }
            })
        }
        
        // public story
        if let uid = FIRAuth.auth()?.currentUser?.uid {
            DataService.ds.REF_SUBSCRIBE.child(uid).observe(.value, with: { (snapshot) in
                var subscribeList = [String]()
                if let snapshot = snapshot.children.allObjects as? [FIRDataSnapshot] {
                    for snap in snapshot {
                        subscribeList.append(snap.key)
                    }
                }
                
                DataService.ds.REF_STORY_PUBLIC.observe(.value, with: { (snapshot) in
                    if let snapshot = snapshot.children.allObjects as? [FIRDataSnapshot] {
                        self.stories.removeAll()
                        for snap in snapshot {
                            if let snapshot2 = snap.children.allObjects as? [FIRDataSnapshot], !subscribeList.contains(snap.key) {
                                var storyArr = [Story]()
                                for story in snapshot2{
                                    if let storyDict = story.value as? Dictionary<String, AnyObject> {
                                        let key = story.key
                                        let story = Story(storyId: key, storyData: storyDict)
                                        storyArr.append(story)
                                    }
                                }
                                DataService.ds.REF_USER_PUBLIC.child(snap.key).observeSingleEvent(of: .value, with: { (snapshot) in
                                    if let userDict = snapshot.value as? Dictionary<String, AnyObject> {
                                        let user = User()
                                        user.id = snapshot.key
                                        user.name = userDict["name"] as? String
                                        let storiesItem = Stories(user: user, storiesData: storyArr)
                                        if let rating = userDict["rating"] as? Int {
                                            storiesItem.rating = rating
                                        } else {
                                            storiesItem.rating = 0
                                        }
                                        self.stories.append(storiesItem)
                                        self.storyTableView?.reloadData()
                                    }
                                })
                            }
                        }
                    }
                })
            })
        }
        
        // topic
        DataService.ds.REF_TOPIC.observeSingleEvent(of: .value, with: { (snapshot) in
            if let snapshot = snapshot.children.allObjects as? [FIRDataSnapshot] {
                for snap in snapshot {
                    if let userDict = snap.value as? Dictionary<String, AnyObject> {
                        var userArr = [String]()
                        for user in userDict {
                            userArr.append(user.key)
                        }
                        self.topic["\(snap.key)"] = userArr
                    }
                }
            }
        })
        
        // friends story
        if let uid = FIRAuth.auth()?.currentUser?.uid {
            DataService.ds.REF_USER_RELATION.child(uid).observe(.value, with: { (snapshot) in
                if let snapshot = snapshot.children.allObjects as? [FIRDataSnapshot] {
                    self.userStories.removeAll()
                    for snap in snapshot {
                        let userID = snap.key
                        DataService.ds.REF_USER.child(userID).observeSingleEvent(of: .value, with: { (snapshot) in
                            if let dictionary = snapshot.value as? [String: AnyObject] {
                                let user = User()
                                user.id = snapshot.key
                                user.setValuesForKeys(dictionary)
                                
                                DataService.ds.REF_USER_STORY.child(userID).observe(.value, with: { (snapshot) in
                                    if let snapshot = snapshot.children.allObjects as? [FIRDataSnapshot] {
                                        let storyArr = [Story]()
                                        let storiesItem = Stories(user: user, storiesData: storyArr)
                                        let index = self.userStories.count
                                        self.userStories.append(storiesItem)
                                        
                                        for snap in snapshot {
                                            
                                            DataService.ds.REF_STORY_PRIVATE.child(snap.key).observeSingleEvent(of: .value, with: { (snapshot) in
                                                if let storyDict = snapshot.value as? Dictionary<String, AnyObject> {
                                                    let key = snapshot.key
                                                    let story = Story(storyId: key, imageUrl: storyDict["imageUrl"] as! String, timestamp: storyDict["timestamp"] as! NSNumber)
                                                        story.title = ""
                                                    self.userStories[index].addStory(story: story)
                                                    self.storyTableView?.reloadData()
                                                }
                                                
                                                
                                            })
                                        }
                                        
                                    }
                                })
                                
                            }
                        })
                    }
                }
            })
        }
        
        // live story
        let endTime = NSDate().timeIntervalSince1970
        let startTime = endTime - 60 * 60 * 3
        
        DataService.ds.REF_STORY_PRIVATE.queryOrdered(byChild: "timestamp").queryStarting(atValue: startTime).queryEnding(atValue: endTime).observe(.value, with: { (snapshot) in
            if let snapshot = snapshot.children.allObjects as? [FIRDataSnapshot] {
                let liveuser = User()
                liveuser.id = "live"
                liveuser.email = "live"
                self.liveStory = Stories(user: liveuser, storiesData: [Story]())
                for snap in snapshot {
                    if let dictionary = snap.value as? [String: AnyObject] {
                        let story = Story(storyId: snap.key, imageUrl: dictionary["imageUrl"] as! String, timestamp: dictionary["timestamp"] as! NSNumber)
                        let date = NSDate(timeIntervalSince1970: story.timestamp as TimeInterval)
                        let dayTimePeriodFormatter = DateFormatter()
                        dayTimePeriodFormatter.dateFormat = "hh:mm a"
                        story.title = "Live @ \(dayTimePeriodFormatter.string(from: date as Date))"
                        self.liveStory?.addStory(story: story)
                    }
                }
            }
        })
    }
    
    func filterStory() {
        filteredStories = stories.filter { (story) -> Bool in
            if searchText == nil || searchText == "" {
                return false
            } else {
                return (story.user?.name?.lowercased().contains((searchText?.lowercased())!))!
            }
        }
    }
    
    func filterTopic() {
        if searchText != nil || searchText != "" {
            filteredTopic.removeAll()
            
            var topicArr = [String]()
            var userArr = [String]()
            for t in self.topic {
                if t.key.lowercased().contains((searchText?.lowercased())!) {
                    topicArr.append(t.key)
                    userArr.append(contentsOf: t.value)
                }
            }
            filteredTopic = stories.filter { (story) -> Bool in
                return userArr.contains((story.user?.id)!)
            }
            
            topicString = topicArr.joined(separator: ",")
        }
    }
    
    func sortByRating() {
        stories.sort {
            $0.rating > $1.rating
        }
    }
    
}

// Collection view extension

extension StoryViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == Sections.subscribe.rawValue {
            return subscribeStories.count
        } else {
            return stories.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == Sections.subscribe.rawValue {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubscribeCell",
                                                          for: indexPath) as! SubscribeCollectionViewCell
            if let story = subscribeStories[indexPath.item].story(at: 0) {
                cell.titleLabel.text = story.title
                cell.userLabel.text = subscribeStories[indexPath.item].user?.name
                cell.backgroundImage.loadImageUsingCacheWithUrlString(urlString: story.media)
                cell.backgroundColor = UIColor.gray
            }
            return cell
        } else {
            sortByRating()
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoryCell",
                                                          for: indexPath) as! StoryCollectionViewCell
            if let story = stories[indexPath.item].story(at: 0) {
                cell.titleLabel.text = story.title
                cell.userLabel.text = stories[indexPath.item].user?.name
                cell.backgroundColor = UIColor.gray
                cell.backgroundImage?.loadImageUsingCacheWithUrlString(urlString: story.media)
            }
            
            return cell
        }
    }
}

extension StoryViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Discover", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "StoryPageVC") as! StoryPageViewController
        if collectionView.tag == Sections.subscribe.rawValue {
            vc.stories = subscribeStories[indexPath.item]
        } else {
            vc.stories = stories[indexPath.item]
            let userID = stories[indexPath.item].user?.id
            DataService.ds.increaseUserRating(userID: userID!, byValue: 1)
        }
        present(vc, animated: true, completion: nil)
    }
}

extension StoryViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == Sections.allStory.rawValue {
            return CGSize(width: 80, height: 109)
        } else {
            return CGSize(width: 110, height: 150)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}

// TableView extension

extension StoryViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if searchText == nil || searchText == "" {
            return 4
        } else {
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchText == nil || searchText == "" {
            switch section {
            case Sections.discover.rawValue:
                return 1
            case Sections.allStory.rawValue:
                let userStoryArr = userStories.filter { (story) -> Bool in
                    if story.story(at: 0) == nil {
                        return false
                    } else {
                        return true
                    }
                }
                return userStoryArr.count
            case Sections.subscribe.rawValue:
                return 1
            case Sections.live.rawValue:
                return 1
            default:
                return 0
            }
        } else {
            if section ==  0 {
                return filteredStories.count
            } else if section == 1 {
                return filteredTopic.count
            } else {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if searchText == nil || searchText == "" {
            if indexPath.section == Sections.discover.rawValue {
                let cell = tableView.dequeueReusableCell(withIdentifier: "storyCell", for: indexPath) as! StoryTableViewCell
                cell.storyCollectionView.tag = Sections.allStory.rawValue
                cell.storyCollectionView.delegate = self
                cell.storyCollectionView.dataSource = self
                cell.storyCollectionView.reloadData()
                return cell
                
            } else if indexPath.section == Sections.subscribe.rawValue {
                let cell = tableView.dequeueReusableCell(withIdentifier: "subscribeCell", for: indexPath) as! SubscribeTableViewCell
                cell.subscribeCollectionView.tag = Sections.subscribe.rawValue
                cell.subscribeCollectionView.delegate = self
                cell.subscribeCollectionView.dataSource = self
                cell.subscribeCollectionView.reloadData()
                return cell
            } else if indexPath.section == Sections.allStory.rawValue {
                let cell = tableView.dequeueReusableCell(withIdentifier: "userStoryCell", for: indexPath) as! UserStoryTableViewCell
                cell.userNameLabel.text = userStories[indexPath.item].user?.name
                if let story = userStories[indexPath.item].story(at: 0) {
                    let date = NSDate(timeIntervalSince1970: story.timestamp as TimeInterval)
                    let dayTimePeriodFormatter = DateFormatter()
                    dayTimePeriodFormatter.dateFormat = "dd MMM hh:mm a"
                    cell.timeLabel.text = dayTimePeriodFormatter.string(from: date as Date)
                    cell.userProfileImage.loadImageUsingCacheWithUrlString(urlString: story.media)
                }
                return cell
            } else if indexPath.section == Sections.live.rawValue {
                let cell = tableView.dequeueReusableCell(withIdentifier: "liveCell", for: indexPath) as! LiveTableViewCell
                
                if let story = liveStory?.story(at: 0) {
                    cell.backgroundColor = UIColor.clear
                    cell.backgroundImage.loadImageUsingCacheWithUrlString(urlString: story.media)
                } else {
                    cell.backgroundColor = UIColor.gray
                }
                return cell
            } else {
                let cell = UITableViewCell()
                return cell
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "userStoryCell", for: indexPath) as! UserStoryTableViewCell
            if indexPath.section == 0 {
                cell.userNameLabel.text = filteredStories[indexPath.item].user?.name
                cell.timeLabel.text = ""
                cell.userProfileImage.loadImageUsingCacheWithUrlString(urlString: (filteredStories[indexPath.item].story(at: 0)?.media)!)
            } else if indexPath.section == 1 {
                cell.userNameLabel.text = filteredTopic[indexPath.item].user?.name
                cell.timeLabel.text = ""
                cell.userProfileImage.loadImageUsingCacheWithUrlString(urlString: (filteredTopic[indexPath.item].story(at: 0)?.media)!)
            }
            return cell
        }
    }
}

extension StoryViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if searchText == nil || searchText == "" {
            if section == Sections.discover.rawValue {
                return 0
            } else {
                return 20
            }
        } else {
            if section == 0, filteredStories.count > 0 {
                return 20
            } else if section == 1, filteredTopic.count > 0 {
                return 20
            } else {
                return 0
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if searchText == nil || searchText == "" {
            if indexPath.section == Sections.discover.rawValue {
                return 120
            } else if indexPath.section == Sections.subscribe.rawValue {
                if subscribeStories.count > 0 {
                    return 160
                } else {
                    return 0
                }
            } else if indexPath.section == Sections.live.rawValue {
                return 160
            } else {
                return 60
            }
        } else {
            return 60
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "titleCell") as! StoryTitleTableViewCell
        var sectionName: String?
        if searchText == nil || searchText == "" {
            if section == Sections.discover.rawValue {
                return nil
            } else if section == Sections.allStory.rawValue {
                let userStoryArr = userStories.filter { (story) -> Bool in
                    if story.story(at: 0) == nil {
                        return false
                    } else {
                        return true
                    }
                }
                if userStoryArr.count > 0 {
                    sectionName = "All stories"
                } else {
                    return nil
                }
            } else if section == Sections.subscribe.rawValue {
                if subscribeStories.count > 0 {
                    sectionName = "Subscriptions"
                } else {
                    return nil
                }
            } else if section == Sections.live.rawValue {
                sectionName = "Live"
            }
        } else {
            if section == Sections.discover.rawValue {
                sectionName = "Discover"
            } else if section == 1, filteredTopic.count > 0 {
                sectionName = "Topic: \(self.topicString)"
            } else {
                return nil
            }
        }
        cell.titleLabel.text = sectionName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Discover", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "StoryPageVC") as! StoryPageViewController
        if indexPath.section == Sections.live.rawValue {
            if liveStory != nil, (liveStory?.size)! > 0 {
                vc.stories = liveStory
                searchController.isActive = false
                present(vc, animated: true, completion: nil)
            }
        } else if indexPath.section == Sections.discover.rawValue {
            if searchText == nil || searchText == "" {
                vc.stories = stories[indexPath.item]
                let userID = stories[indexPath.item].user?.id
                DataService.ds.increaseUserRating(userID: userID!, byValue: 1)
                
            } else {
                vc.stories = filteredStories[indexPath.item]
                let userID = filteredStories[indexPath.item].user?.id
                DataService.ds.increaseUserRating(userID: userID!, byValue: 1)
            }
            searchController.isActive = false
            present(vc, animated: true, completion: nil)
        } else if indexPath.section == Sections.allStory.rawValue {
            if searchText == nil || searchText == "" {
                vc.stories = userStories[indexPath.item]
            } else {
                vc.stories = filteredTopic[indexPath.item]
            }
            searchController.isActive = false
            present(vc, animated: true, completion: nil)
        }
    }
}

extension StoryViewController: UISearchResultsUpdating, UISearchControllerDelegate, UISearchBarDelegate {
    func updateSearchResults(for searchController: UISearchController) {
        searchText = searchController.searchBar.text!
    }
}


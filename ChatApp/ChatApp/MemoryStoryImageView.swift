//
//  MemoryStoryImageView.swift
//  ChatApp
//
//  Created by Fan Qi on 16/10/17.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import UIKit

class MemoryStoryImageView: UIImageView {
    
    var memoryStory: MemoryStory? {
        didSet {
            if let imageUrl = memoryStory?.dict?.first?.value as? String {
                loadImageUsingCacheWithUrlString(urlString: imageUrl)
            }
        }
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

//
//  MapViewController.swift
//  ChatApp
//
//  Created by Fan Qi on 16/10/19.
//  Copyright © 2016年 Melbourne University. All rights reserved.
//

import UIKit
import MapKit


class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    var editingController: EditingController?
    
    var locationManager = CLLocationManager()
    
    lazy var cancelButton: UIButton = {
        let button = UIButton(type: UIButtonType.system)
        button.setTitle("X", for: UIControlState.normal)
        button.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor(r: 21, g: 139, b: 201), for: UIControlState.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        button.addTarget(self, action: #selector(handleCancel(_:)), for: UIControlEvents.touchUpInside)
        return button
    }()
    
    lazy var saveButton: UIButton = {
        let button = UIButton()
        button.setTitle("S", for: UIControlState.normal)
        button.addTarget(self, action: #selector(handleSave(_:)), for: UIControlEvents.touchUpInside)
        button.backgroundColor = UIColor.clear
        button.setTitleColor(UIColor(r: 21, g: 139, b: 201), for: UIControlState.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        return button
    }()

    
    @IBOutlet weak var mapView: MKMapView! {
        didSet{
            mapView.mapType = .standard
            mapView.delegate = self
            mapView.showsUserLocation = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        cancelButton.frame = CGRect(x: 20, y: 10, width: 40, height: 50)
        saveButton.frame = CGRect(x: view.bounds.width - 70, y: 10, width: 50, height: 50)
            
        view.addSubview(cancelButton)
        view.addSubview(saveButton)
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        self.mapView.setRegion(region, animated: true)
        self.locationManager.stopUpdatingLocation()
    }
    
    func handleSave(_ sender: UIButton) {
        let renderer = UIGraphicsImageRenderer(size: self.view.bounds.size)
        let image = renderer.image { (ctx) in
            self.mapView.drawHierarchy(in: self.view.bounds, afterScreenUpdates: true)
        }
        dismiss(animated: false) {
            DispatchQueue.main.async {
                self.editingController?.backGroundImageView.image = image
            }
        }
    }
    
    func handleCancel(_ sender: UIButton) {
        dismiss(animated: false, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */


}

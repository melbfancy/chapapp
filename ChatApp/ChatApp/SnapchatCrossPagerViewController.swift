//
//  SnapchatCrossPagerViewController.swift
//  SnapchatCrossPager
//
//  Created by Paride on 13/09/2016. Modified by Qi Fan and Chang Hsien Liu.
//  Copyright © 2016 paridebroggi. All rights reserved.
//

import UIKit
import Firebase

protocol SnapchatCrossPagerDelegate {
    func scrollViewShouldScroll() -> Bool
}

class SnapchatCrossPagerViewController: PagerViewController {
    
    var alertController: UIAlertController?
    
    override var topVC: UIViewController {
        return verticalPager.topVC
    }
    
    override var bottomVC: UIViewController {
        return verticalPager.bottomVC
    }
    
    override var centerVC: UIViewController {
        return verticalPager.centerVC
    }
    
    var verticalPager: PagerViewController
    var crossPagerDelegate: SnapchatCrossPagerDelegate?
    
    init(leftVC: UIViewController, centerVC: UIViewController, rightVC: UIViewController, topVC: UIViewController, bottomVC: UIViewController, endVC: UIViewController) {
        verticalPager = PagerViewController(scrollDirection: .vertical, beforeVC: topVC, centerVC: centerVC, afterVC: bottomVC, endVC: topVC)
        super.init(beforeVC: leftVC, centerVC: verticalPager, afterVC: rightVC, endVC: endVC)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        crossPagerDelegate = verticalPager
        verticalPager.scrollView.delegate = self
        checkIfUserIsLoggedIn()
    }
    
    func checkIfUserIsLoggedIn(){
        // user is not logged in
        if FIRAuth.auth()?.currentUser?.uid == nil {
            perform(#selector(handleLogout), with: nil, afterDelay: 0)
        }
    }
    
    func handleLogout(){
        // log out
        try? FIRAuth.auth()?.signOut()
        
        let loginController = LoginController()
        present(loginController, animated: false, completion: nil)
    }
    
    func scrollToPage(page: Int) {
        let boundSize = self.scrollView.bounds.size
        let frame = CGRect(x: boundSize.width * CGFloat(page), y: 0, width: boundSize.width, height: boundSize.height)
        self.scrollView.scrollRectToVisible(frame, animated: true)

    }
    
    func handleNetworkConnected() {
        alertController?.dismiss(animated: true, completion: nil)
    }
    
    func handleNetworkDisconnected() {
        alertController =  UIAlertController(title: "Network Error", message: "Device not connected to the internet", preferredStyle: UIAlertControllerStyle.alert)
        present(alertController!, animated: true, completion: nil)
    }
    
}

//MARK: - SCROLLVIEW DELEGATE
extension SnapchatCrossPagerViewController: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let crossPagerDelegate = crossPagerDelegate {
            self.scrollView.isScrollEnabled = crossPagerDelegate.scrollViewShouldScroll()
        }
    }
    
}
